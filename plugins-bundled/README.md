Bundled Plugins
===============

Bundled plugins are built and treated as internal Core plugins. While they get built and bundled as part of the CI release, they are not built automatically when building locally.


### Build
In order to build locally, you can run from the app root

```
yarn run plugins:build-bundled
```

This will iteratively build plugins (frontent and backend)  found under `plugins-bundled/internal` and bundle them in the final artifacts.

### Developing

When developing a plugin, you should be able to make changes to the plugin frontend without having to reload the whole app. For instance you could watch and build a plugin by doing :

```
cd PLUGIN
yarn start # this will automatically build and serve the plugin frontend
```

If making changes to the plugin backend on the other hand, you would need to rebuild the app backend to have new changes.

```
cd PLUGIN
yarn run build-backend

cd APP_ROOT
make run
```
### Clean
To clean/remove plugins:

```
plugins:clean-bundled
```

