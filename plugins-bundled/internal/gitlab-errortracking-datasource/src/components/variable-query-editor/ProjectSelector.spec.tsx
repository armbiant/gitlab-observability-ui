import React from 'react';
import { render, waitFor } from '@testing-library/react';
import { DataSourceInstanceSettings } from '@grafana/data';
import { ProjectSelector } from './ProjectSelector';
import { SentryDataSource } from './../../datasource';
import { SentryConfig } from './../../types';

jest.mock('@grafana/runtime', () => ({
  ...((jest.requireActual('@grafana/runtime') as unknown) as object),
  getTemplateSrv: () => ({
    replace: (s: string) => s,
    getVariables: () => [],
  }),
}));

describe('ProjectSelector', () => {
  it('should render without error', () => {
    const datasource = new SentryDataSource({} as DataSourceInstanceSettings<SentryConfig>);
    datasource.postResource = jest.fn(() => Promise.resolve([]));
    datasource.getProjects = jest.fn(() => Promise.resolve([]));
    const onChange = jest.fn();
    const result = render(
      <ProjectSelector datasource={datasource} onValuesChange={onChange} mode="id" orgSlug="foo" values={[]} />
    );
    waitFor(() => {
      expect(result.container.firstChild).not.toBeNull();
    });
  });
});
