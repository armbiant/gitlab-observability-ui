package sentry_test

import (
	"errors"
	"fmt"
	"net/http"
	"testing"

	"github.com/grafana/grafana-plugin-sdk-go/build"
	"github.com/stretchr/testify/require"
	"gitlab-errortracking-datasource/pkg/sentry"
)

const pluginId string = "gitlab-errortracking-datasource"
const dummyVersion string = "dummy-version"
const dummyPath string = "/some-path"
const authToken string = "dummy-auth-token"
const gobCookiePresent bool = false

func TestHttpClient(t *testing.T) {
	t.Run("it attaches the auth header", func(t *testing.T) {
		client := &fakeHttpClient{}
		c := sentry.NewHTTPClient(client, pluginId, stubBuildInfoProvider, authToken, gobCookiePresent)
		req, err := http.NewRequest(http.MethodGet, dummyPath, nil)
		require.Nil(t, err)
		_, _ = c.Do(req)
		require.Equal(t, "Bearer "+authToken, client.req.Header.Get("Authorization"))
	})

	t.Run("it sets a custom user agent with the plugin header", func(t *testing.T) {
		client := &fakeHttpClient{}
		c := sentry.NewHTTPClient(client, pluginId, stubBuildInfoProvider, authToken, gobCookiePresent)
		req, err := http.NewRequest(http.MethodGet, dummyPath, nil)
		require.Nil(t, err)
		_, _ = c.Do(req)
		require.Equal(t, fmt.Sprintf("%s/%s", pluginId, dummyVersion), client.req.UserAgent())
	})

	t.Run("it sets the version to 'unknown' when the buildInfoProvider returns an error", func(t *testing.T) {
		provider := func() (build.Info, error) {
			return build.Info{}, errors.New("500 Internal server error")
		}
		client := &fakeHttpClient{}
		c := sentry.NewHTTPClient(client, pluginId, provider, authToken, gobCookiePresent)
		req, err := http.NewRequest(http.MethodGet, dummyPath, nil)
		require.Nil(t, err)
		_, _ = c.Do(req)
		require.Equal(t, fmt.Sprintf("%s/%s", pluginId, "unknown-version"), client.req.UserAgent())
	})

	t.Run("in case GOB cookie is not present in headers but required by datasource setting", func(t *testing.T) {
		client := &fakeHttpClient{}
		c := sentry.NewHTTPClient(client, pluginId, stubBuildInfoProvider, authToken, true)
		req, err := http.NewRequest(http.MethodGet, dummyPath, nil)
		require.Nil(t, err)
		_, err = c.Do(req)
		require.Error(t, err)
	})

	t.Run("don't attach the auth header in case GOB cookie is present", func(t *testing.T) {
		client := &fakeHttpClient{}
		c := sentry.NewHTTPClient(client, pluginId, stubBuildInfoProvider, authToken, true)
		c.AttachHeaders(map[string]string{
			"Cookie": fmt.Sprintf("%s=blah", sentry.GobCookieName),
		})
		req, err := http.NewRequest(http.MethodGet, dummyPath, nil)
		require.Nil(t, err)
		_, err = c.Do(req)
		require.Empty(t, client.req.Header.Get("Authorization"))
	})
}

func stubBuildInfoProvider() (build.Info, error) {
	return build.Info{
		Version: dummyVersion,
	}, nil
}

type fakeHttpClient struct {
	req *http.Request
}

func (c *fakeHttpClient) Do(req *http.Request) (*http.Response, error) {
	c.req = req
	return nil, nil
}
