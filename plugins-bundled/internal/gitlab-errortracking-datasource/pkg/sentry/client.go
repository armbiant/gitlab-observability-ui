package sentry

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/grafana/grafana-plugin-sdk-go/backend"
	"github.com/grafana/grafana-plugin-sdk-go/build"
)

type doer interface {
	Do(*http.Request) (*http.Response, error)
}

const GobCookieName = "gob.sid"

// HTTP creates an HTTP client with a 'Do' method. It automatically injects
// the provided api token into every request with an `Authorization: Bearer AuthToken` header
type HTTPClient struct {
	doer
	pluginId  string
	version   string
	authToken string
	// Passed headers from the plugin backend
	Headers            map[string]string
	isGobCookiePresent bool
}

// BuildInfoProvider is a function that returns the build.Info for the plugin
type BuildInfoProvider func() (build.Info, error)

// NewHTTPClient creates a new AuthHTTP client
func NewHTTPClient(
	d doer,
	pluginId string,
	b BuildInfoProvider,
	authToken string,
	isGobCookiePresent bool,
) *HTTPClient {
	info, err := b()
	version := info.Version
	if err != nil {
		version = "unknown-version"
	}
	return &HTTPClient{
		doer:               d,
		pluginId:           pluginId,
		version:            version,
		authToken:          authToken,
		Headers:            make(map[string]string),
		isGobCookiePresent: isGobCookiePresent,
	}
}

// AttachHeaders sticks the passed headers to HTTPClient (which are used when making all requests)
func (a *HTTPClient) AttachHeaders(headers map[string]string) {
	a.Headers = headers
}

// Do attaches the sentry authentication header and the User-Agent header to
// the request and passes it to the injected http Doer
// Also attach the provided headers from the backend
func (a *HTTPClient) Do(req *http.Request) (*http.Response, error) {
	req.Header.Set("User-Agent", fmt.Sprintf("%s/%s", a.pluginId, a.version))

	// If GOB cookie is present, don't add authorization header
	if !a.isGobCookiePresent {
		req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", a.authToken))
	}

	for header, value := range a.Headers {
		// If `value = a.Headers["Cookie]` has GobCookie; only set it if required in datasource setting
		// This will ignore setting this cookie on requests being made from gitlab-errortracking datasource
		// to other sentry URLs.
		if strings.ToLower(header) == "cookie" {
			if strings.Index(value, GobCookieName) != -1 && a.isGobCookiePresent {
				backend.Logger.Info("setting gob cookie on header as it required by setting")
				req.Header.Set(header, value)
			}
		} else {
			req.Header.Set(header, value)
		}
	}
	if _, ok := a.Headers["Cookie"]; !ok && a.isGobCookiePresent {
		return nil, fmt.Errorf("cookie not present but required by datasource settings")
	}
	return a.doer.Do(req)
}
