package plugin

const (
	PluginID                     string = "gitlab-errortracking-datasource"
	SuccessfulHealthCheckMessage string = "plugin health check successful"
	DefaultSentryURL             string = "https://sentry.io"
)
