package plugin

import (
	"encoding/json"

	"github.com/grafana/grafana-plugin-sdk-go/backend"
)

const GobCookieName = "gob.sid"

type SentryConfig struct {
	URL                string   `json:"url"`
	OrgSlug            string   `json:"orgSlug"`
	authToken          string   `json:"-"`
	KeepCookies        []string `json:"keepCookies"`
	InsecureSkipVerify bool     `json:"tlsSkipVerify"`
	IsGOBCookiePresent bool
}

func (sc *SentryConfig) Validate() error {
	if sc.URL == "" {
		return ErrorInvalidSentryConfig
	}
	if sc.OrgSlug == "" {
		return ErrorInvalidOrganizationSlug
	}
	if !sc.IsGOBCookiePresent && sc.authToken == "" {
		backend.Logger.Error(
			"auth token required since gob cookie is not present",
			"error",
			ErrorInvalidAuthToken.Error())
		return ErrorInvalidAuthToken
	}
	return nil
}

func GetSettings(s backend.DataSourceInstanceSettings) (*SentryConfig, error) {
	config := &SentryConfig{}
	if err := json.Unmarshal(s.JSONData, config); err != nil {
		backend.Logger.Error(ErrorUnmarshalingSettings.Error())
		return nil, ErrorUnmarshalingSettings
	}
	if config.URL == "" {
		backend.Logger.Info("applying default sentry URL", "sentry url", DefaultSentryURL)
		config.URL = DefaultSentryURL
	}
	if config.OrgSlug == "" {
		return nil, ErrorInvalidOrganizationSlug
	}
	if authToken, ok := s.DecryptedSecureJSONData["authToken"]; ok {
		config.authToken = authToken
	}
	if config.KeepCookies != nil && len(config.KeepCookies) > 0 {
		for _, name := range config.KeepCookies {
			if name == GobCookieName {
				config.IsGOBCookiePresent = true
			}
		}
	}
	if config.IsGOBCookiePresent {
		backend.Logger.Info("GOB cookie present in the datasource setting")
	}
	return config, config.Validate()
}
