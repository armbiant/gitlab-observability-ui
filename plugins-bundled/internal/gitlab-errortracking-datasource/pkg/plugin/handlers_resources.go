package plugin

import (
	"context"
	"encoding/json"
	"net/http"
	"strings"

	"gitlab-errortracking-datasource/pkg/sentry"

	"github.com/grafana/grafana-plugin-sdk-go/backend"
	"github.com/grafana/grafana-plugin-sdk-go/backend/resource"
)

const SentryResourceQueryTypeOrganizations = "organizations"
const SentryResourceQueryTypeProjects = "projects"

type SentryResourceQuery struct {
	Type    string `json:"type"`
	OrgSlug string `json:"orgSlug"`
}

func GetResourceQuery(body []byte) (*SentryResourceQuery, error) {
	query := SentryResourceQuery{}
	if err := json.Unmarshal(body, &query); err != nil {
		return nil, ErrorFailedUnmarshalingResourceQuery
	}
	if query.Type == "" {
		return nil, ErrorInvalidResourceCallQuery
	}
	return &query, nil
}

func (ds *SentryDatasource) CallResource(ctx context.Context, req *backend.CallResourceRequest, sender backend.CallResourceResponseSender) error {
	if strings.ToUpper(req.Method) == http.MethodPost {
		dsi, err := ds.getDatasourceInstance(ctx, req.PluginContext)
		if err != nil {
			return err
		}

		// NOTE(prozlach): http://www.w3.org/Protocols/rfc2616/rfc2616-sec4.html#sec4.2
		//
		// Multiple message-header fields with the same field-name MAY be
		// present in a message if and only if the entire field-value for that
		// header field is defined as a comma-separated list [i.e., #(values)].
		// It MUST be possible to combine the multiple header fields into one
		// "field-name: field-value" pair, without changing the semantics of
		// the message, by appending each subsequent field-value to the first,
		// each separated by a comma. The order in which header fields with the
		// same field-name are received is therefore significant to the
		// interpretation of the combined field value, and thus a proxy MUST
		// NOT change the order of these field values when a message is
		// forwarded.
		tmp := make(map[string]string)
		for k, s := range req.Headers {
			tmp[k] = strings.Join(s, ",")
		}
		dsi.sentryClient.AttachHeaders(tmp)

		query, err := GetResourceQuery(req.Body)
		if err != nil {
			return err
		}
		o, err := CallResource(ctx, dsi.sentryClient, *query)
		if err != nil {
			return err
		}
		return resource.SendJSON(sender, o)
	}
	return ErrorInvalidResourceCallQuery
}

func CallResource(ctx context.Context, sentryClient sentry.SentryClient, query SentryResourceQuery) (interface{}, error) {
	switch query.Type {
	case SentryResourceQueryTypeOrganizations:
		return sentryClient.GetOrganizations()
	case SentryResourceQueryTypeProjects:
		if query.OrgSlug == "" {
			return nil, ErrorInvalidOrganizationSlug
		}
		return sentryClient.GetProjects(query.OrgSlug)
	default:
		return nil, ErrorInvalidResourceCallQuery
	}
}
