import { promises as fsPromises } from 'fs';
import style from '@grafana/ui/src/themes/gitlab-ui-overrides/gitlab_ui_overrides.scss';
import { generateGitLabUIOverrides } from '@grafana/ui/src/themes/gitlab-ui-overrides/generateOverride';
const outputFile = __dirname + '/../../../../packages/grafana-ui/src/themes/gitlab-ui-overrides/overrides.generated.ts';

const generateFiles = async () => {
  try {
    await fsPromises.writeFile(outputFile, generateGitLabUIOverrides(style));
    console.log(`Gitlab UI override file generated at ${outputFile}.`);
  } catch (error) {
    console.error(`\nWriting Gitlab UI override file failed at ${outputFile}`, error);
    throw error;
  }
};

generateFiles();
