#!/bin/bash

# shellcheck disable=SC2086

#
#   This script is executed from within the container.
#
set -eou pipefail

##########
CCARMV7=""
#CCARMV7_MUSL=""
CCARM64=""
#CCARM64_MUSL=""
CCX64=""
#CCX64_MUSL=""

BUILD_BACKEND=0
BUILD_BACKEND_ALL=0
BUILD_FRONTEND=0
BUILD_PACKAGE=0
BUILD_PACKAGE_ALL=0
BUILD_ALL=0
BUILD_DOCKER=0
BUILD_DOCKER_REGISTRY=""
OPT=""

while [ $# -ge 1 ]; do
  case "${1-default}" in
    "--build-args")
      OPT=$2
      echo "Build arguments set"
      shift 2
      ;;
    "--docker")
      BUILD_DOCKER=1
      echo "Build docker multiarch container"
      shift
      BUILD_DOCKER_REGISTRY=$1
      shift
      ;;
    "--all")
      BUILD_ALL=1
      echo "Building everything - frontend, backend, packaging, docker images"
      shift
      ;;
    "--backend")
      BUILD_BACKEND=1
      echo "Building only backend for x86_64"
      shift
      ;;
    "--backend-all")
      BUILD_BACKEND_ALL=1
      echo "Building only backend for all archs"
      shift
      ;;
    "--frontend")
      BUILD_FRONTEND=1
      echo "Building only frontend"
      shift
      ;;
    "--package")
      BUILD_PACKAGE=1
      echo "Building only packaging for x86_64"
      shift
      ;;
    "--package-all")
      BUILD_PACKAGE_ALL=1
      echo "Building only packaging for all archs"
      shift
      ;;
    * )
      # unknown param causes args to be passed through to $@
      break
      ;;
  esac
done

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd $SCRIPT_DIR/../../
echo "repository directory: $(pwd)"

if [[ -n "${OPT}" ]]
then
    echo "Build arguments: ${OPT}"
fi

function build_backend_linux_amd64() {
  if [ ! -d "dist" ]; then
    mkdir dist
  fi
  CC=${CCX64} go run build.go ${OPT} build-server
  #CC=${CCX64_MUSL} go run build.go -libc musl ${OPT} build-server
}

function build_backend() {
  if [ ! -d "dist" ]; then
    mkdir dist
  fi

  go run build.go -goarch armv7 -cc ${CCARMV7} ${OPT} build-server
  go run build.go -goarch arm64 -cc ${CCARM64} ${OPT} build-server
  #go run build.go -goarch armv7 -libc musl -cc ${CCARMV7_MUSL} ${OPT} build-server
  #go run build.go -goarch arm64 -libc musl -cc ${CCARM64_MUSL} ${OPT} build-server
  build_backend_linux_amd64
}

function build_frontend() {
  if [ ! -d "dist" ]; then
    mkdir dist
  fi
  echo "Building frontend"

  start=$(date +%s%N)
  go run build.go ${OPT} build-frontend
  runtime=$((($(date +%s%N) - start)/1000000))
  echo "Frontend build took: $runtime ms"
  echo "FRONTEND: finished"
}

function package_linux_amd64() {
  echo "Packaging Linux AMD64"
  go run build.go -goos linux -goarch amd64 ${OPT} package-only
  #go run build.go -goos linux -goarch amd64 ${OPT} -libc musl package-only
  echo "PACKAGE LINUX AMD64: finished"
}

function package_all() {
  echo "Packaging ALL"
  go run build.go -goos linux -goarch armv7 ${OPT} package-only
  go run build.go -goos linux -goarch arm64 ${OPT} package-only
  #go run build.go -goos linux -goarch armv7 -libc musl ${OPT} package-only
  #go run build.go -goos linux -goarch arm64 -libc musl ${OPT} package-only
  package_linux_amd64
  echo "PACKAGE ALL: finished"
}

function package_setup() {
  echo "Packaging: Setup environment"
  if [ -d "dist" ]; then
    rm -rf dist
  fi
  mkdir dist
  go run build.go -gen-version ${OPT} > dist/grafana.version
}

function verify_build_chain_amd64() {

    if ! command -v x86_64-linux-gnu-gcc &> /dev/null
    then
        echo "GCC amd64 compiler has not been found"
        exit 1
    fi
    CCX64=$(which x86_64-linux-gnu-gcc)

    # if ! command -v x86_64-linux-musl-gcc &> /dev/null
    # then
    #     echo "MUSL amd64 compiler has not been found"
    #     exit 1
    # fi
    #CCX64_MUSL=$(which x86_64-linux-musl-gcc)
}

function verify_build_chain() {
    verify_build_chain_amd64

    if ! command -v arm-linux-gnueabihf-gcc &> /dev/null
    then
        echo "GCC armv7 compiler has not been found"
        exit 1
    fi
    CCARMV7=$(which arm-linux-gnueabihf-gcc)

    if ! command -v aarch64-linux-gnu-gcc &> /dev/null
    then
        echo "GCC arm64 compiler has not been found"
        exit 1
    fi
    CCARM64=$(which aarch64-linux-gnu-gcc)

    # if ! command -v armv7l-linux-musleabihf-gcc &> /dev/null
    # then
    #     echo "GCC armv7 compiler has not been found"
    #     exit 1
    # fi
    #CCARMV7_MUSL=$(which armv7l-linux-musleabihf-gcc)

    # if ! command -v aarch64-linux-musl-gcc &> /dev/null
    # then
    #     echo "MUSL arm64 compiler has not been found"
    #     exit 1
    # fi
    #CCARM64_MUSL=$(which aarch64-linux-musl-gcc)
}

function build_docker() {
    DOCKER_REGISTRY=$1
    if [ -z "$DOCKER_REGISTRY" ]
    then
        echo "Docker is currently unable to use multi-arch images locally, a push to registry is needed first."
        echo "See https://github.com/docker/buildx/issues/59#issuecomment-1189999088"
        echo "Please specify registry as an argument to '--docker' switch and try again"
        exit 1
    fi

    VERSION=$(cat dist/grafana.version)
    cp -v dist/grafana-${VERSION}.*.tar.gz packaging/docker/
    cd packaging/docker/
    # TODO(prozlach): keeping tagging schema when developing locally very
    # simple for now. Some more thought and discussion is needed here.
    docker buildx build \
        -t $DOCKER_REGISTRY \
        --platform linux/amd64,linux/arm64,linux/arm/v7 \
        --push \
            .
    rm -v grafana-*.tar.gz
}

if [ $BUILD_ALL = "1" ]
then
  verify_build_chain
  build_backend
  build_frontend
  package_setup
  package_all
fi
if [ $BUILD_BACKEND = "1" ]; then
  verify_build_chain_amd64
  build_backend_linux_amd64
fi
if [ $BUILD_BACKEND_ALL = "1" ]; then
  verify_build_chain
  build_backend
fi
if [ $BUILD_FRONTEND = "1" ]; then
  build_frontend
fi
if [ $BUILD_PACKAGE = "1" ]; then
  package_setup
  package_linux_amd64
fi
if [ $BUILD_PACKAGE_ALL = "1" ]; then
  package_setup
  package_all
fi
if [ $BUILD_DOCKER = "1" ]; then
  build_docker $BUILD_DOCKER_REGISTRY
fi
