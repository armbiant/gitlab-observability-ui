module github.com/grafana/grafana/scripts/go

go 1.14

require (
	github.com/golangci/golangci-lint v1.44.2
	github.com/mgechev/revive v1.1.4
	github.com/unknwon/bra v0.0.0-20200517080246-1e3013ecaff8
	github.com/unknwon/log v0.0.0-20200308114134-929b1006e34a // indirect
	github.com/urfave/cli v1.22.5 // indirect
)
