import { promises as fs } from 'fs';
import { Task, TaskRunner } from '../task';
import execa = require('execa');

interface BundeManagedOptions {}

const MANAGED_PLUGINS_PATH = `${process.cwd()}/plugins-bundled`;
const MANAGED_PLUGINS_SCOPES = ['internal' /* 'external' Not currently supporting external plugins*/];

const bundleManagedPluginsRunner: TaskRunner<BundeManagedOptions> = async () => {
  await Promise.all(
    MANAGED_PLUGINS_SCOPES.map(async (scope) => {
      const plugins = (await fs.readdir(`${MANAGED_PLUGINS_PATH}/${scope}`, { withFileTypes: true })).flatMap((d) =>
        d.isDirectory() ? d.name : []
      );

      if (plugins.length > 0) {
        for (const plugin of plugins) {
          console.log(`[${scope}]: ${plugin} building...`);
          await execa('yarn', ['build'], { cwd: `${MANAGED_PLUGINS_PATH}/${scope}/${plugin}` });
          console.log(`[${scope}]: ${plugin} bundled`);
        }
      }
    })
  );
};

export const bundleManagedTask = new Task<BundeManagedOptions>('Bundle managed plugins', bundleManagedPluginsRunner);
