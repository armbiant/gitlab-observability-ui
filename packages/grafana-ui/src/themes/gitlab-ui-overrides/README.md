# GitLab UI Overrides

In order to be able to import `@gitlab/ui` Sass variables into GOUI and use them from either Sass stylesheets or TS modules/React components, they first need to be pre-processed by the `scripts/cli/import-gitlab-ui` script at build time.

The script runs automatically when running `yarn start` or it can also be launched manually by running `yarn themes:generate`.

## How to import Sass variables from `@gitlab/ui`

In order to import a Sass variable from `@gitlab/ui` and use it in GOUI, you need to:

1. Import and re-export the variable in `gitlab_ui_overrides.scss`. (Note the exported object must be flat, without nested objects )

```scss
@use '@gitlab/ui/src/scss/variables' as gitlab_ui_variables;

$some-variable = gitlab_ui_variables.$font-family-monospace;

:export {
    someVariable: $some-variable
}
```

For a list of available variables from `@gitlab/ui` check out https://gitlab.com/gitlab-org/gitlab-ui/-/blob/main/doc/css.md#overriding-variables

2. Add the exported variable to the type definition file in `gitlab_ui_overrides.scss.d.ts`

```ts
export interface SassExport {
  ...
  someVariable: string;
}
```

3. Run `yarn themes:generate` ( while running `yarn start` this will be executed automatically when making changes to `gitlab_ui_overrides.scss`)

4. Use the newly added variable from `gitlab-ui-overrides` module in the `theme` object in `themes/default.ts` ( or `themes/dark.ts`, `themes/light.ts`)

```js
import { gitlabUI } from './gitlab-ui-overrides';

const theme = {
    ....
    someVar: gitlabUI.someVariable
}
```

5. (optional) If you need to expose the variable to Sass stylesheets, you can add it to the Sass variables template generator in `_variables.scss.tmpl.ts`.

```ts
export const commonThemeVarsTemplate = (theme) =>
  `
...
$some-variable: ${theme.someVar};
...
`;
```
