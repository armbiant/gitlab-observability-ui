export interface SassExport {
  fontFamilySansSerif: string;
  fontFamilyMonoSpace: string;
}

declare const style: SassExport;
export default style;
