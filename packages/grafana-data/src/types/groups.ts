export interface UserGroupDTO {
  groupId: number;
  name: string;
  role: GroupRole;
}

export enum GroupRole {
  Admin = 'Admin',
  Editor = 'Editor',
  Viewer = 'Viewer',
}
