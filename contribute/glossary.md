# Glossary

## Acronyms

**GCP:** Google Cloud Platform

**GOB:** GitLab Observability Backend

**GOP:** GitLab Observability Platform (GOB + GOUI)

**GOUI:** GitLab Observability User Interface

## Tools

**Booter:** The program that runs on the devvm during startup to install all the components of GOP.

**ClickHouse:** Open source column-oriented DBMS for online analytical processing. ClickHouse was selected as an appropriate solution Database for GOB.

**devvm:** A virtual machine, fully configured with the GitLab Observability Platform (GOP). This provides a working system for development and experimentation.

**Gatekeeper:** Responsible for authentication, authorization, and creating `GitLabNamespace` objects.

**gcloud:** Command line tools for managing Google Cloud (GCP) resources.

**Jaeger:** Open source tool for distributed transaction monitoring.

**kubectl:** Command line tool for communicating with a Kubernetes cluster's control plane.

**OTel (OpenTelemetry) collector:** Gathers traces for presentation by Jaeger UI.

**Prometheus:** Open source tool for monitoring.

**Redis:** In-memory data structure store, used for caching and session storage for Gatekeeper.

**Sentry:** Sentry is an error tracking platform. GitLab leverages the open source Sentry SDK to send errors to GitLab.
