This software is based on Grafana:
Copyright 2014-2021 Grafana Labs

----

This software is based on Kibana: 
Copyright 2012-2013 Elasticsearch BV

----

Portions Copyright 2022-present GitLab B.V.
