//go:build integration
// +build integration

package sqlstore

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/models"
)

func TestGetDBHealthQuery(t *testing.T) {
	InitTestDB(t)

	query := models.GetDBHealthQuery{}
	err := GetDBHealthQuery(&query)
	require.NoError(t, err)
}
