package sqlstore

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/bus"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/events"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/models"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/setting"
	"xorm.io/xorm"
)

// MainGroupName is the name of the main organization.
const MainGroupName = "Main Group"

func init() {
	bus.AddHandler("sql", GetOrgById)
	bus.AddHandler("sql", CreateOrg)
	bus.AddHandler("sql", UpdateOrg)
	bus.AddHandler("sql", UpdateOrgAddress)
	bus.AddHandler("sql", GetOrgByName)
	bus.AddHandler("sql", SearchOrgs)
	bus.AddHandler("sql", DeleteOrg)
}

func SearchOrgs(query *models.SearchGroupsQuery) error {
	query.Result = make([]*models.GroupDTO, 0)
	sess := x.Table("grp")
	if query.Query != "" {
		sess.Where("name LIKE ?", query.Query+"%")
	}
	if query.Name != "" {
		sess.Where("name=?", query.Name)
	}

	if len(query.Ids) > 0 {
		sess.In("id", query.Ids)
	}

	if query.Limit > 0 {
		sess.Limit(query.Limit, query.Limit*query.Page)
	}

	sess.Cols("id", "name")
	err := sess.Find(&query.Result)
	return err
}

func GetOrgById(query *models.GetGroupByIdQuery) error {
	var group models.Grp
	exists, err := x.Id(query.Id).Get(&group)
	if err != nil {
		return err
	}

	if !exists {
		return models.ErrGroupNotFound
	}

	query.Result = &group
	return nil
}

func GetOrgByName(query *models.GetGroupByNameQuery) error {
	var group models.Grp
	exists, err := x.Where("name=?", query.Name).Get(&group)
	if err != nil {
		return err
	}

	if !exists {
		return models.ErrGroupNotFound
	}

	query.Result = &group
	return nil
}

// GetOrgByName gets an organization by name.
func (ss *SQLStore) GetOrgByName(name string) (*models.Grp, error) {
	var group models.Grp
	exists, err := ss.engine.Where("name=?", name).Get(&group)
	if err != nil {
		return nil, err
	}
	if !exists {
		return nil, models.ErrGroupNotFound
	}

	return &group, nil
}

func isOrgNameTaken(name string, existingId int64, sess *DBSession) (bool, error) {
	// check if org name is taken
	var group models.Grp
	exists, err := sess.Where("name=?", name).Get(&group)

	if err != nil {
		return false, nil
	}

	if exists && existingId != group.Id {
		return true, nil
	}

	return false, nil
}

func createOrg(name string, userID int64, engine *xorm.Engine, id int64) (models.Grp, error) {
	group := models.Grp{
		Id:      id,
		Name:    name,
		Created: time.Now(),
		Updated: time.Now(),
	}
	if err := inTransactionWithRetryCtx(context.Background(), engine, func(sess *DBSession) error {
		if isNameTaken, err := isOrgNameTaken(name, 0, sess); err != nil {
			return err
		} else if isNameTaken {
			return models.ErrGroupNameTaken
		}

		if _, err := sess.Insert(&group); err != nil {
			return err
		}

		user := models.GroupUser{
			GroupId: group.Id,
			UserId:  userID,
			Role:    models.ROLE_ADMIN,
			Created: time.Now(),
			Updated: time.Now(),
		}

		_, err := sess.Insert(&user)

		sess.publishAfterCommit(&events.OrgCreated{
			Timestamp: group.Created,
			Id:        group.Id,
			Name:      group.Name,
		})

		return err
	}, 0); err != nil {
		return group, err
	}

	return group, nil
}

// CreateOrgWithMember creates an organization with a certain name and a certain user as member.
func (ss *SQLStore) CreateOrgWithMember(name string, userID int64, orgId int64) (models.Grp, error) {
	return createOrg(name, userID, ss.engine, orgId)
}

func CreateOrg(cmd *models.CreateGroupCommand) error {
	org, err := createOrg(cmd.Name, cmd.UserId, x, cmd.Id)
	if err != nil {
		return err
	}

	cmd.Result = org
	return nil
}

func UpdateOrg(cmd *models.UpdateGroupCommand) error {
	return inTransaction(func(sess *DBSession) error {
		if isNameTaken, err := isOrgNameTaken(cmd.Name, cmd.GroupId, sess); err != nil {
			return err
		} else if isNameTaken {
			return models.ErrGroupNameTaken
		}

		group := models.Grp{
			Name:    cmd.Name,
			Updated: time.Now(),
		}

		affectedRows, err := sess.ID(cmd.GroupId).Update(&group)

		if err != nil {
			return err
		}

		if affectedRows == 0 {
			return models.ErrGroupNotFound
		}

		sess.publishAfterCommit(&events.OrgUpdated{
			Timestamp: group.Updated,
			Id:        group.Id,
			Name:      group.Name,
		})

		return nil
	})
}

func UpdateOrgAddress(cmd *models.UpdateGroupAddressCommand) error {
	return inTransaction(func(sess *DBSession) error {
		group := models.Grp{
			Address1: cmd.Address1,
			Address2: cmd.Address2,
			City:     cmd.City,
			ZipCode:  cmd.ZipCode,
			State:    cmd.State,
			Country:  cmd.Country,

			Updated: time.Now(),
		}

		if _, err := sess.ID(cmd.GroupId).Update(&group); err != nil {
			return err
		}

		sess.publishAfterCommit(&events.OrgUpdated{
			Timestamp: group.Updated,
			Id:        group.Id,
			Name:      group.Name,
		})

		return nil
	})
}

func DeleteOrg(cmd *models.DeleteGroupCommand) error {
	return inTransaction(func(sess *DBSession) error {
		if res, err := sess.Query("SELECT 1 from grp WHERE id=?", cmd.Id); err != nil {
			return err
		} else if len(res) != 1 {
			return models.ErrGroupNotFound
		}

		deletes := []string{
			"DELETE FROM star WHERE EXISTS (SELECT 1 FROM dashboard WHERE group_id = ? AND star.dashboard_id = dashboard.id)",
			"DELETE FROM dashboard_tag WHERE EXISTS (SELECT 1 FROM dashboard WHERE group_id = ? AND dashboard_tag.dashboard_id = dashboard.id)",
			"DELETE FROM dashboard WHERE group_id = ?",
			"DELETE FROM api_key WHERE group_id = ?",
			"DELETE FROM data_source WHERE group_id = ?",
			"DELETE FROM group_user WHERE group_id = ?",
			"DELETE FROM grp WHERE id = ?",
			"DELETE FROM temp_user WHERE group_id = ?",
		}

		for _, sql := range deletes {
			_, err := sess.Exec(sql, cmd.Id)
			if err != nil {
				return err
			}
		}

		return nil
	})
}

func verifyExistingOrg(sess *DBSession, orgId int64) error {
	var group models.Grp
	has, err := sess.Where("id=?", orgId).Get(&group)
	if err != nil {
		return err
	}
	if !has {
		return models.ErrGroupNotFound
	}
	return nil
}

func (ss *SQLStore) getOrCreateOrg(sess *DBSession, groupName string) (int64, error) {
	var group models.Grp
	if ss.Cfg.AutoAssignOrg {
		has, err := sess.Where("id=?", ss.Cfg.AutoAssignOrgId).Get(&group)
		if err != nil {
			return 0, err
		}
		if has {
			return group.Id, nil
		}

		if ss.Cfg.AutoAssignOrgId != 1 {
			ss.log.Error("Could not create user: group ID does not exist", "groupId",
				ss.Cfg.AutoAssignOrgId)
			return 0, fmt.Errorf("could not create user: group ID %d does not exist",
				ss.Cfg.AutoAssignOrgId)
		}

		group.Name = MainGroupName
		group.Id = int64(ss.Cfg.AutoAssignOrgId)
	} else {
		group.Name = groupName
	}

	group.Created = time.Now()
	group.Updated = time.Now()

	if group.Id != 0 {
		if _, err := sess.InsertId(&group); err != nil {
			return 0, err
		}
	} else {
		if _, err := sess.InsertOne(&group); err != nil {
			return 0, err
		}
	}

	sess.publishAfterCommit(&events.OrgCreated{
		Timestamp: group.Created,
		Id:        group.Id,
		Name:      group.Name,
	})

	return group.Id, nil
}

func getOrCreateOrg(sess *DBSession, groupName string) (int64, error) {
	var group models.Grp
	if setting.AutoAssignOrg {
		has, err := sess.Where("id=?", setting.AutoAssignOrgId).Get(&group)
		if err != nil {
			return 0, err
		}
		if has {
			return group.Id, nil
		}

		if setting.AutoAssignOrgId != 1 {
			sqlog.Error("Could not create user: organization ID does not exist", "orgID",
				setting.AutoAssignOrgId)
			return 0, fmt.Errorf("could not create user: organization ID %d does not exist",
				setting.AutoAssignOrgId)
		}

		group.Name = MainGroupName
		group.Id = int64(setting.AutoAssignOrgId)
	} else {
		group.Name = groupName
	}
	group.Created = time.Now()
	group.Updated = time.Now()

	if group.Id != 0 {
		if _, err := sess.InsertId(&group); err != nil {
			return 0, err
		}
	} else {
		if _, err := sess.InsertOne(&group); err != nil {
			return 0, err
		}
	}
	sess.publishAfterCommit(&events.OrgCreated{
		Timestamp: group.Created,
		Id:        group.Id,
		Name:      group.Name,
	})

	return group.Id, nil
}
