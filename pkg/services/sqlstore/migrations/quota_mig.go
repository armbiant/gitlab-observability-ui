package migrations

import (
	. "gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/services/sqlstore/migrator"
)

func addQuotaMigration(mg *Migrator) {
	var quotaV1 = Table{
		Name: "quota",
		Columns: []*Column{
			{Name: "id", Type: DB_BigInt, IsPrimaryKey: true, IsAutoIncrement: true},
			{Name: "org_id", Type: DB_BigInt, Nullable: true},
			{Name: "user_id", Type: DB_BigInt, Nullable: true},
			{Name: "target", Type: DB_NVarchar, Length: 190, Nullable: false},
			{Name: "limit", Type: DB_BigInt, Nullable: false},
			{Name: "created", Type: DB_DateTime, Nullable: false},
			{Name: "updated", Type: DB_DateTime, Nullable: false},
		},
		Indices: []*Index{
			{Cols: []string{"org_id", "user_id", "target"}, Type: UniqueIndex},
		},
	}
	mg.AddMigration("create quota table v1", NewAddTableMigration(quotaV1))

	//-------  indexes ------------------
	addTableIndicesMigrations(mg, "v1", quotaV1)

	mg.AddMigration("Update quota table charset", NewTableCharsetMigration("quota", []*Column{
		{Name: "target", Type: DB_NVarchar, Length: 190, Nullable: false},
	}))

	// ---------------------
	// org -> group changes

	// drop indexes
	addDropAllIndicesMigrations(mg, "v1", quotaV1)

	// rename table
	addTableRenameMigration(mg, "quota", "quota_v2", "v2")

	quotaV2 := Table{
		Name: "quota",
		Columns: []*Column{
			{Name: "id", Type: DB_BigInt, IsPrimaryKey: true, IsAutoIncrement: true},
			{Name: "group_id", Type: DB_BigInt, Nullable: true},
			{Name: "user_id", Type: DB_BigInt, Nullable: true},
			{Name: "target", Type: DB_NVarchar, Length: 190, Nullable: false},
			{Name: "limit", Type: DB_BigInt, Nullable: false},
			{Name: "created", Type: DB_DateTime, Nullable: false},
			{Name: "updated", Type: DB_DateTime, Nullable: false},
		},
		Indices: []*Index{
			{Cols: []string{"group_id", "user_id", "target"}, Type: UniqueIndex},
		},
	}

	// create v2 table
	mg.AddMigration("create quota table v2", NewAddTableMigration(quotaV2))

	// add v2 indíces
	addTableIndicesMigrations(mg, "v2", quotaV2)

	//------- copy data from v1 to v2 -------------------
	mg.AddMigration("copy quota v1 to v2", NewCopyTableDataMigration("quota", "quota_v2", map[string]string{
		"id":       "id",
		"group_id": "org_id",
		"user_id":  "user_id",
		"target":   "target",
		"limit":    "limit",
		"created":  "created",
		"updated":  "updated",
	}))

	mg.AddMigration("Drop old table quota_v2", NewDropTableMigration("quota_v2"))
}
