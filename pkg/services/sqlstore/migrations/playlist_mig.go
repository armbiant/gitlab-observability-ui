package migrations

import . "gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/services/sqlstore/migrator"

func addPlaylistMigrations(mg *Migrator) {
	mg.AddMigration("Drop old table playlist table", NewDropTableMigration("playlist"))
	mg.AddMigration("Drop old table playlist_item table", NewDropTableMigration("playlist_item"))

	playlistV2 := Table{
		Name: "playlist",
		Columns: []*Column{
			{Name: "id", Type: DB_BigInt, IsPrimaryKey: true, IsAutoIncrement: true},
			{Name: "name", Type: DB_NVarchar, Length: 255, Nullable: false},
			{Name: "interval", Type: DB_NVarchar, Length: 255, Nullable: false},
			{Name: "org_id", Type: DB_BigInt, Nullable: false},
		},
	}

	// create table
	mg.AddMigration("create playlist table v2", NewAddTableMigration(playlistV2))

	playlistItemV2 := Table{
		Name: "playlist_item",
		Columns: []*Column{
			{Name: "id", Type: DB_BigInt, IsPrimaryKey: true, IsAutoIncrement: true},
			{Name: "playlist_id", Type: DB_BigInt, Nullable: false},
			{Name: "type", Type: DB_NVarchar, Length: 255, Nullable: false},
			{Name: "value", Type: DB_Text, Nullable: false},
			{Name: "title", Type: DB_Text, Nullable: false},
			{Name: "order", Type: DB_Int, Nullable: false},
		},
	}

	mg.AddMigration("create playlist item table v2", NewAddTableMigration(playlistItemV2))

	mg.AddMigration("Update playlist table charset", NewTableCharsetMigration("playlist", []*Column{
		{Name: "name", Type: DB_NVarchar, Length: 255, Nullable: false},
		{Name: "interval", Type: DB_NVarchar, Length: 255, Nullable: false},
	}))

	mg.AddMigration("Update playlist_item table charset", NewTableCharsetMigration("playlist_item", []*Column{
		{Name: "type", Type: DB_NVarchar, Length: 255, Nullable: false},
		{Name: "value", Type: DB_Text, Nullable: false},
		{Name: "title", Type: DB_Text, Nullable: false},
	}))

	// ---------------------
	// org -> group changes

	// rename table
	addTableRenameMigration(mg, "playlist", "playlist_v2", "v2")

	playlistV3 := Table{
		Name: "playlist",
		Columns: []*Column{
			{Name: "id", Type: DB_BigInt, IsPrimaryKey: true, IsAutoIncrement: true},
			{Name: "name", Type: DB_NVarchar, Length: 255, Nullable: false},
			{Name: "interval", Type: DB_NVarchar, Length: 255, Nullable: false},
			{Name: "group_id", Type: DB_BigInt, Nullable: false},
		},
	}

	// create v3 table
	mg.AddMigration("create playlist table v3", NewAddTableMigration(playlistV3))

	//------- copy data from v2 to v3 -------------------
	mg.AddMigration("copy playlist v2 to v3", NewCopyTableDataMigration("playlist", "playlist_v2", map[string]string{
		"id":       "id",
		"name":     "name",
		"interval": "interval",
		"group_id": "org_id",
	}))

	mg.AddMigration("Drop old table playlist_v2", NewDropTableMigration("playlist_v2"))
}
