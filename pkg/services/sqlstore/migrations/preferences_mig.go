package migrations

import . "gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/services/sqlstore/migrator"

func addPreferencesMigrations(mg *Migrator) {
	mg.AddMigration("drop preferences table v2", NewDropTableMigration("preferences"))

	preferencesV2 := Table{
		Name: "preferences",
		Columns: []*Column{
			{Name: "id", Type: DB_BigInt, IsPrimaryKey: true, IsAutoIncrement: true},
			{Name: "org_id", Type: DB_BigInt, Nullable: false},
			{Name: "user_id", Type: DB_BigInt, Nullable: false},
			{Name: "version", Type: DB_Int, Nullable: false},
			{Name: "home_dashboard_id", Type: DB_BigInt, Nullable: false},
			{Name: "timezone", Type: DB_NVarchar, Length: 50, Nullable: false},
			{Name: "theme", Type: DB_NVarchar, Length: 20, Nullable: false},
			{Name: "created", Type: DB_DateTime, Nullable: false},
			{Name: "updated", Type: DB_DateTime, Nullable: false},
		},
		Indices: []*Index{
			{Cols: []string{"org_id"}},
			{Cols: []string{"user_id"}},
		},
	}

	mg.AddMigration("drop preferences table v3", NewDropTableMigration("preferences"))

	// create table
	mg.AddMigration("create preferences table v3", NewAddTableMigration(preferencesV2))

	mg.AddMigration("Update preferences table charset", NewTableCharsetMigration("preferences", []*Column{
		{Name: "timezone", Type: DB_NVarchar, Length: 50, Nullable: false},
		{Name: "theme", Type: DB_NVarchar, Length: 20, Nullable: false},
	}))

	mg.AddMigration("Add column team_id in preferences", NewAddColumnMigration(preferencesV2, &Column{
		Name: "team_id", Type: DB_BigInt, Nullable: true,
	}))

	mg.AddMigration("Update team_id column values in preferences", NewRawSQLMigration("").
		SQLite("UPDATE preferences SET team_id=0 WHERE team_id IS NULL;").
		Postgres("UPDATE preferences SET team_id=0 WHERE team_id IS NULL;").
		Mysql("UPDATE preferences SET team_id=0 WHERE team_id IS NULL;"))

	// ---------------------
	// org -> group changes

	// drop indexes
	addDropAllIndicesMigrations(mg, "v2", preferencesV2)

	// rename table
	addTableRenameMigration(mg, "preferences", "preferences_v2", "v2")

	preferencesV3 := Table{
		Name: "preferences",
		Columns: []*Column{
			{Name: "id", Type: DB_BigInt, IsPrimaryKey: true, IsAutoIncrement: true},
			{Name: "group_id", Type: DB_BigInt, Nullable: false},
			{Name: "user_id", Type: DB_BigInt, Nullable: false},
			{Name: "version", Type: DB_Int, Nullable: false},
			{Name: "home_dashboard_id", Type: DB_BigInt, Nullable: false},
			{Name: "timezone", Type: DB_NVarchar, Length: 50, Nullable: false},
			{Name: "theme", Type: DB_NVarchar, Length: 20, Nullable: false},
			{Name: "created", Type: DB_DateTime, Nullable: false},
			{Name: "updated", Type: DB_DateTime, Nullable: false},
			{Name: "team_id", Type: DB_BigInt, Nullable: true},
		},
		Indices: []*Index{
			{Cols: []string{"group_id"}},
			{Cols: []string{"user_id"}},
		},
	}

	// create v2 table
	mg.AddMigration("create preferences table v3", NewAddTableMigration(preferencesV3))

	// add v2 indíces
	addTableIndicesMigrations(mg, "v3", preferencesV3)

	//------- copy data from v2 to v3 -------------------
	mg.AddMigration("copy preferences v2 to v3", NewCopyTableDataMigration("preferences", "preferences_v2", map[string]string{
		"id":                "id",
		"group_id":          "org_id",
		"user_id":           "user_id",
		"version":           "version",
		"home_dashboard_id": "home_dashboard_id",
		"timezone":          "timezone",
		"theme":             "theme",
		"created":           "created",
		"updated":           "updated",
		"team_id":           "team_id",
	}))

	mg.AddMigration("Drop old table preferences_v2", NewDropTableMigration("preferences_v2"))
}
