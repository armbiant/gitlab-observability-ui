//go:build integration
// +build integration

package sqlstore

import (
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/models"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/setting"
)

func TestQuotaCommandsAndQueries(t *testing.T) {
	InitTestDB(t)
	userId := int64(1)
	orgId := int64(0)

	setting.Quota = setting.QuotaSettings{
		Enabled: true,
		Org: &setting.OrgQuota{
			User:       5,
			Dashboard:  5,
			DataSource: 5,
			ApiKey:     5,
		},
		User: &setting.UserQuota{
			Org: 5,
		},
		Global: &setting.GlobalQuota{
			Org:        5,
			User:       5,
			Dashboard:  5,
			DataSource: 5,
			ApiKey:     5,
			Session:    5,
		},
	}

	// create a new org and add user_id 1 as admin.
	// we will then have an org with 1 user. and a user
	// with 1 org.
	userCmd := models.CreateGroupCommand{
		Name:   "TestOrg",
		UserId: 1,
	}

	err := CreateOrg(&userCmd)
	require.NoError(t, err)
	orgId = userCmd.Result.Id

	t.Run("Given saved org quota for users", func(t *testing.T) {
		orgCmd := models.UpdateOrgQuotaCmd{
			GroupId: orgId,
			Target:  "group_user",
			Limit:   10,
		}
		err := UpdateOrgQuota(&orgCmd)
		require.NoError(t, err)

		t.Run("Should be able to get saved quota by org id and target", func(t *testing.T) {
			query := models.GetOrgQuotaByTargetQuery{GroupId: orgId, Target: "group_user", Default: 1}
			err = GetOrgQuotaByTarget(&query)

			require.NoError(t, err)
			require.Equal(t, int64(10), query.Result.Limit)
		})

		t.Run("Should be able to get default quota by org id and target", func(t *testing.T) {
			query := models.GetOrgQuotaByTargetQuery{GroupId: 123, Target: "group_user", Default: 11}
			err = GetOrgQuotaByTarget(&query)

			require.NoError(t, err)
			require.Equal(t, int64(11), query.Result.Limit)
		})

		t.Run("Should be able to get used org quota when rows exist", func(t *testing.T) {
			query := models.GetOrgQuotaByTargetQuery{GroupId: orgId, Target: "group_user", Default: 11}
			err = GetOrgQuotaByTarget(&query)

			require.NoError(t, err)
			require.Equal(t, int64(1), query.Result.Used)
		})

		t.Run("Should be able to get used org quota when no rows exist", func(t *testing.T) {
			query := models.GetOrgQuotaByTargetQuery{GroupId: 2, Target: "group_user", Default: 11}
			err = GetOrgQuotaByTarget(&query)

			require.NoError(t, err)
			require.Equal(t, int64(0), query.Result.Used)
		})

		t.Run("Should be able to quota list for org", func(t *testing.T) {
			query := models.GetOrgQuotasQuery{GroupId: orgId}
			err = GetOrgQuotas(&query)

			require.NoError(t, err)
			require.Len(t, query.Result, 4)
			for _, res := range query.Result {
				limit := int64(5) // default quota limit
				used := int64(0)
				if res.Target == "group_user" {
					limit = 10 // customized quota limit.
					used = 1
				}
				require.Equal(t, limit, res.Limit)
				require.Equal(t, used, res.Used)
			}
		})
	})

	t.Run("Given saved org quota for dashboards", func(t *testing.T) {
		orgCmd := models.UpdateOrgQuotaCmd{
			GroupId: orgId,
			Target:  dashboardTarget,
			Limit:   10,
		}
		err := UpdateOrgQuota(&orgCmd)
		require.NoError(t, err)

		t.Run("Should be able to get saved quota by org id and target", func(t *testing.T) {
			query := models.GetOrgQuotaByTargetQuery{GroupId: orgId, Target: dashboardTarget, Default: 1}
			err = GetOrgQuotaByTarget(&query)

			require.NoError(t, err)
			require.Equal(t, int64(10), query.Result.Limit)
			require.Equal(t, int64(0), query.Result.Used)
		})
	})

	t.Run("Given saved user quota for org", func(t *testing.T) {
		userQuotaCmd := models.UpdateUserQuotaCmd{
			UserId: userId,
			Target: "group_user",
			Limit:  10,
		}
		err := UpdateUserQuota(&userQuotaCmd)
		require.NoError(t, err)

		t.Run("Should be able to get saved quota by user id and target", func(t *testing.T) {
			query := models.GetUserQuotaByTargetQuery{UserId: userId, Target: "group_user", Default: 1}
			err = GetUserQuotaByTarget(&query)

			require.NoError(t, err)
			require.Equal(t, int64(10), query.Result.Limit)
		})

		t.Run("Should be able to get default quota by user id and target", func(t *testing.T) {
			query := models.GetUserQuotaByTargetQuery{UserId: 9, Target: "group_user", Default: 11}
			err = GetUserQuotaByTarget(&query)

			require.NoError(t, err)
			require.Equal(t, int64(11), query.Result.Limit)
		})

		t.Run("Should be able to get used user quota when rows exist", func(t *testing.T) {
			query := models.GetUserQuotaByTargetQuery{UserId: userId, Target: "group_user", Default: 11}
			err = GetUserQuotaByTarget(&query)

			require.NoError(t, err)
			require.Equal(t, int64(1), query.Result.Used)
		})

		t.Run("Should be able to get used user quota when no rows exist", func(t *testing.T) {
			query := models.GetUserQuotaByTargetQuery{UserId: 2, Target: "group_user", Default: 11}
			err = GetUserQuotaByTarget(&query)

			require.NoError(t, err)
			require.Equal(t, int64(0), query.Result.Used)
		})

		t.Run("Should be able to quota list for user", func(t *testing.T) {
			query := models.GetUserQuotasQuery{UserId: userId}
			err = GetUserQuotas(&query)

			require.NoError(t, err)
			require.Len(t, query.Result, 1)
			require.Equal(t, int64(10), query.Result[0].Limit)
			require.Equal(t, int64(1), query.Result[0].Used)
		})
	})

	t.Run("Should be able to global user quota", func(t *testing.T) {
		query := models.GetGlobalQuotaByTargetQuery{Target: "user", Default: 5}
		err = GetGlobalQuotaByTarget(&query)
		require.NoError(t, err)

		require.Equal(t, int64(5), query.Result.Limit)
		require.Equal(t, int64(0), query.Result.Used)
	})

	t.Run("Should be able to global org quota", func(t *testing.T) {
		query := models.GetGlobalQuotaByTargetQuery{Target: "grp", Default: 5}
		err = GetGlobalQuotaByTarget(&query)
		require.NoError(t, err)

		require.Equal(t, int64(5), query.Result.Limit)
		require.Equal(t, int64(1), query.Result.Used)
	})

	t.Run("Should be able to global dashboard quota", func(t *testing.T) {
		query := models.GetGlobalQuotaByTargetQuery{Target: dashboardTarget, Default: 5}
		err = GetGlobalQuotaByTarget(&query)
		require.NoError(t, err)

		require.Equal(t, int64(5), query.Result.Limit)
		require.Equal(t, int64(0), query.Result.Used)
	})

	// related: https://gitlab.com/gitlab-org/opstrace/opstrace-ui/issues/14342
	t.Run("Should org quota updating is successful even if it called multiple time", func(t *testing.T) {
		orgCmd := models.UpdateOrgQuotaCmd{
			GroupId: orgId,
			Target:  "group_user",
			Limit:   5,
		}
		err := UpdateOrgQuota(&orgCmd)
		require.NoError(t, err)

		query := models.GetOrgQuotaByTargetQuery{GroupId: orgId, Target: "group_user", Default: 1}
		err = GetOrgQuotaByTarget(&query)
		require.NoError(t, err)
		require.Equal(t, int64(5), query.Result.Limit)

		// XXX: resolution of `Updated` column is 1sec, so this makes delay
		time.Sleep(1 * time.Second)

		orgCmd = models.UpdateOrgQuotaCmd{
			GroupId: orgId,
			Target:  "group_user",
			Limit:   10,
		}
		err = UpdateOrgQuota(&orgCmd)
		require.NoError(t, err)

		query = models.GetOrgQuotaByTargetQuery{GroupId: orgId, Target: "group_user", Default: 1}
		err = GetOrgQuotaByTarget(&query)
		require.NoError(t, err)
		require.Equal(t, int64(10), query.Result.Limit)
	})

	// related: https://gitlab.com/gitlab-org/opstrace/opstrace-ui/issues/14342
	t.Run("Should user quota updating is successful even if it called multiple time", func(t *testing.T) {
		userQuotaCmd := models.UpdateUserQuotaCmd{
			UserId: userId,
			Target: "group_user",
			Limit:  5,
		}
		err := UpdateUserQuota(&userQuotaCmd)
		require.NoError(t, err)

		query := models.GetUserQuotaByTargetQuery{UserId: userId, Target: "group_user", Default: 1}
		err = GetUserQuotaByTarget(&query)
		require.NoError(t, err)
		require.Equal(t, int64(5), query.Result.Limit)

		// XXX: resolution of `Updated` column is 1sec, so this makes delay
		time.Sleep(1 * time.Second)

		userQuotaCmd = models.UpdateUserQuotaCmd{
			UserId: userId,
			Target: "group_user",
			Limit:  10,
		}
		err = UpdateUserQuota(&userQuotaCmd)
		require.NoError(t, err)

		query = models.GetUserQuotaByTargetQuery{UserId: userId, Target: "group_user", Default: 1}
		err = GetUserQuotaByTarget(&query)
		require.NoError(t, err)
		require.Equal(t, int64(10), query.Result.Limit)
	})
}
