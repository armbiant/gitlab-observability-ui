//go:build integration
// +build integration

package sqlstore

import (
	"context"
	"fmt"
	"testing"
	"time"

	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/models"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/setting"
)

func TestAccountDataAccess(t *testing.T) {
	Convey("Testing Account DB Access", t, func() {
		InitTestDB(t)

		Convey("Given we have organizations, we can query them by IDs", func() {
			var err error
			var cmd *models.CreateGroupCommand
			ids := []int64{}

			for i := 1; i < 4; i++ {
				cmd = &models.CreateGroupCommand{Name: fmt.Sprint("Org #", i)}
				err = CreateOrg(cmd)
				So(err, ShouldBeNil)

				ids = append(ids, cmd.Result.Id)
			}

			query := &models.SearchGroupsQuery{Ids: ids}
			err = SearchOrgs(query)

			So(err, ShouldBeNil)
			So(len(query.Result), ShouldEqual, 3)
		})

		Convey("Given we have organizations, we can limit and paginate search", func() {
			for i := 1; i < 4; i++ {
				cmd := &models.CreateGroupCommand{Name: fmt.Sprint("Org #", i)}
				err := CreateOrg(cmd)
				So(err, ShouldBeNil)
			}

			Convey("Should be able to search with defaults", func() {
				query := &models.SearchGroupsQuery{}
				err := SearchOrgs(query)

				So(err, ShouldBeNil)
				So(len(query.Result), ShouldEqual, 3)
			})

			Convey("Should be able to limit search", func() {
				query := &models.SearchGroupsQuery{Limit: 1}
				err := SearchOrgs(query)

				So(err, ShouldBeNil)
				So(len(query.Result), ShouldEqual, 1)
			})

			Convey("Should be able to limit and paginate search", func() {
				query := &models.SearchGroupsQuery{Limit: 2, Page: 1}
				err := SearchOrgs(query)

				So(err, ShouldBeNil)
				So(len(query.Result), ShouldEqual, 1)
			})
		})

		Convey("Given single org mode", func() {
			setting.AutoAssignOrg = true
			setting.AutoAssignOrgId = 1
			setting.AutoAssignOrgRole = "Viewer"

			Convey("Users should be added to default organization", func() {
				ac1cmd := models.CreateUserCommand{Login: "ac1", Email: "ac1@test.com", Name: "ac1 name"}
				ac2cmd := models.CreateUserCommand{Login: "ac2", Email: "ac2@test.com", Name: "ac2 name"}

				err := CreateUser(context.Background(), &ac1cmd)
				So(err, ShouldBeNil)
				err = CreateUser(context.Background(), &ac2cmd)
				So(err, ShouldBeNil)

				q1 := models.GetUserOrgListQuery{UserId: ac1cmd.Result.Id}
				q2 := models.GetUserOrgListQuery{UserId: ac2cmd.Result.Id}
				err = GetUserOrgList(&q1)
				So(err, ShouldBeNil)
				err = GetUserOrgList(&q2)
				So(err, ShouldBeNil)

				So(q1.Result[0].GroupId, ShouldEqual, q2.Result[0].GroupId)
				So(q1.Result[0].Role, ShouldEqual, "Viewer")
			})
		})

		Convey("Given two saved users", func() {
			setting.AutoAssignOrg = false

			ac1cmd := models.CreateUserCommand{Login: "ac1", Email: "ac1@test.com", Name: "ac1 name"}
			ac2cmd := models.CreateUserCommand{Login: "ac2", Email: "ac2@test.com", Name: "ac2 name", IsAdmin: true}

			err := CreateUser(context.Background(), &ac1cmd)
			err = CreateUser(context.Background(), &ac2cmd)
			So(err, ShouldBeNil)

			ac1 := ac1cmd.Result
			ac2 := ac2cmd.Result

			Convey("Should be able to read user info projection", func() {
				query := models.GetUserProfileQuery{UserId: ac1.Id}
				err = GetUserProfile(&query)

				So(err, ShouldBeNil)
				So(query.Result.Email, ShouldEqual, "ac1@test.com")
				So(query.Result.Login, ShouldEqual, "ac1")
			})

			Convey("Can search users", func() {
				query := models.SearchUsersQuery{Query: ""}
				err := SearchUsers(&query)

				So(err, ShouldBeNil)
				So(query.Result.Users[0].Email, ShouldEqual, "ac1@test.com")
				So(query.Result.Users[1].Email, ShouldEqual, "ac2@test.com")
			})

			Convey("Given an added org user", func() {
				cmd := models.AddGroupUserCommand{
					GroupId: ac1.GroupId,
					UserId:  ac2.Id,
					Role:    models.ROLE_VIEWER,
				}

				err := AddOrgUser(&cmd)
				Convey("Should have been saved without error", func() {
					So(err, ShouldBeNil)
				})

				Convey("Can update org user role", func() {
					updateCmd := models.UpdateGroupUserCommand{GroupId: ac1.GroupId, UserId: ac2.Id, Role: models.ROLE_ADMIN}
					err = UpdateOrgUser(&updateCmd)
					So(err, ShouldBeNil)

					orgUsersQuery := models.GetGroupUsersQuery{GroupId: ac1.GroupId}
					err = GetOrgUsers(&orgUsersQuery)
					So(err, ShouldBeNil)

					So(orgUsersQuery.Result[1].Role, ShouldEqual, models.ROLE_ADMIN)
				})

				Convey("Can get logged in user projection", func() {
					query := models.GetSignedInUserQuery{UserId: ac2.Id}
					err := GetSignedInUser(&query)

					So(err, ShouldBeNil)
					So(query.Result.Email, ShouldEqual, "ac2@test.com")
					So(query.Result.GroupId, ShouldEqual, ac2.GroupId)
					So(query.Result.Name, ShouldEqual, "ac2 name")
					So(query.Result.Login, ShouldEqual, "ac2")
					So(query.Result.OrgRole, ShouldEqual, "Admin")
					So(query.Result.GroupName, ShouldEqual, "ac2@test.com")
					So(query.Result.IsGrafanaAdmin, ShouldBeTrue)
				})

				Convey("Can get user organizations", func() {
					query := models.GetUserOrgListQuery{UserId: ac2.Id}
					err := GetUserOrgList(&query)

					So(err, ShouldBeNil)
					So(len(query.Result), ShouldEqual, 2)
				})

				Convey("Can get organization users", func() {
					query := models.GetGroupUsersQuery{GroupId: ac1.GroupId}
					err := GetOrgUsers(&query)

					So(err, ShouldBeNil)
					So(len(query.Result), ShouldEqual, 2)
					So(query.Result[0].Role, ShouldEqual, "Admin")
				})

				Convey("Can get organization users with query", func() {
					query := models.GetGroupUsersQuery{
						GroupId: ac1.GroupId,
						Query:   "ac1",
					}
					err := GetOrgUsers(&query)

					So(err, ShouldBeNil)
					So(len(query.Result), ShouldEqual, 1)
					So(query.Result[0].Email, ShouldEqual, ac1.Email)
				})

				Convey("Can get organization users with query and limit", func() {
					query := models.GetGroupUsersQuery{
						GroupId: ac1.GroupId,
						Query:   "ac",
						Limit:   1,
					}
					err := GetOrgUsers(&query)

					So(err, ShouldBeNil)
					So(len(query.Result), ShouldEqual, 1)
					So(query.Result[0].Email, ShouldEqual, ac1.Email)
				})

				Convey("Can set using org", func() {
					cmd := models.SetUsingOrgCommand{UserId: ac2.Id, GroupId: ac1.GroupId}
					err := SetUsingOrg(&cmd)
					So(err, ShouldBeNil)

					Convey("SignedInUserQuery with a different org", func() {
						query := models.GetSignedInUserQuery{UserId: ac2.Id}
						err := GetSignedInUser(&query)

						So(err, ShouldBeNil)
						So(query.Result.GroupId, ShouldEqual, ac1.GroupId)
						So(query.Result.Email, ShouldEqual, "ac2@test.com")
						So(query.Result.Name, ShouldEqual, "ac2 name")
						So(query.Result.Login, ShouldEqual, "ac2")
						So(query.Result.GroupName, ShouldEqual, "ac1@test.com")
						So(query.Result.OrgRole, ShouldEqual, "Viewer")
					})

					Convey("Should set last org as current when removing user from current", func() {
						remCmd := models.RemoveGroupUserCommand{GroupId: ac1.GroupId, UserId: ac2.Id}
						err := RemoveOrgUser(&remCmd)
						So(err, ShouldBeNil)

						query := models.GetSignedInUserQuery{UserId: ac2.Id}
						err = GetSignedInUser(&query)

						So(err, ShouldBeNil)
						So(query.Result.GroupId, ShouldEqual, ac2.GroupId)
					})
				})

				Convey("Removing user from group should delete user completely if in no other org", func() {
					// make sure ac2 has no org
					err := DeleteOrg(&models.DeleteGroupCommand{Id: ac2.GroupId})
					So(err, ShouldBeNil)

					// remove ac2 user from ac1 org
					remCmd := models.RemoveGroupUserCommand{GroupId: ac1.GroupId, UserId: ac2.Id, ShouldDeleteOrphanedUser: true}
					err = RemoveOrgUser(&remCmd)
					So(err, ShouldBeNil)
					So(remCmd.UserWasDeleted, ShouldBeTrue)

					err = GetSignedInUser(&models.GetSignedInUserQuery{UserId: ac2.Id})
					So(err, ShouldEqual, models.ErrUserNotFound)
				})

				Convey("Cannot delete last admin org user", func() {
					cmd := models.RemoveGroupUserCommand{GroupId: ac1.GroupId, UserId: ac1.Id}
					err := RemoveOrgUser(&cmd)
					So(err, ShouldEqual, models.ErrLastGroupAdmin)
				})

				Convey("Cannot update role so no one is admin user", func() {
					cmd := models.UpdateGroupUserCommand{GroupId: ac1.GroupId, UserId: ac1.Id, Role: models.ROLE_VIEWER}
					err := UpdateOrgUser(&cmd)
					So(err, ShouldEqual, models.ErrLastGroupAdmin)
				})

				Convey("Given an org user with dashboard permissions", func() {
					ac3cmd := models.CreateUserCommand{Login: "ac3", Email: "ac3@test.com", Name: "ac3 name", IsAdmin: false}
					err := CreateUser(context.Background(), &ac3cmd)
					So(err, ShouldBeNil)
					ac3 := ac3cmd.Result

					orgUserCmd := models.AddGroupUserCommand{
						GroupId: ac1.GroupId,
						UserId:  ac3.Id,
						Role:    models.ROLE_VIEWER,
					}

					err = AddOrgUser(&orgUserCmd)
					So(err, ShouldBeNil)

					query := models.GetGroupUsersQuery{GroupId: ac1.GroupId}
					err = GetOrgUsers(&query)
					So(err, ShouldBeNil)
					So(len(query.Result), ShouldEqual, 3)

					dash1 := insertTestDashboard(t, "1 test dash", ac1.GroupId, 0, false, "prod", "webapp")
					dash2 := insertTestDashboard(t, "2 test dash", ac3.GroupId, 0, false, "prod", "webapp")

					err = testHelperUpdateDashboardAcl(dash1.Id, models.DashboardAcl{DashboardID: dash1.Id, GroupId: ac1.GroupId, UserID: ac3.Id, Permission: models.PERMISSION_EDIT})
					So(err, ShouldBeNil)

					err = testHelperUpdateDashboardAcl(dash2.Id, models.DashboardAcl{DashboardID: dash2.Id, GroupId: ac3.GroupId, UserID: ac3.Id, Permission: models.PERMISSION_EDIT})
					So(err, ShouldBeNil)

					Convey("When group user is deleted", func() {
						cmdRemove := models.RemoveGroupUserCommand{GroupId: ac1.GroupId, UserId: ac3.Id}
						err := RemoveOrgUser(&cmdRemove)
						So(err, ShouldBeNil)

						Convey("Should remove dependent permissions for deleted group user", func() {
							permQuery := &models.GetDashboardAclInfoListQuery{DashboardID: 1, GroupId: ac1.GroupId}
							err = GetDashboardAclInfoList(permQuery)
							So(err, ShouldBeNil)

							So(len(permQuery.Result), ShouldEqual, 0)
						})

						Convey("Should not remove dashboard permissions for same user in another group", func() {
							permQuery := &models.GetDashboardAclInfoListQuery{DashboardID: 2, GroupId: ac3.GroupId}
							err = GetDashboardAclInfoList(permQuery)
							So(err, ShouldBeNil)

							So(len(permQuery.Result), ShouldEqual, 1)
							So(permQuery.Result[0].GroupId, ShouldEqual, ac3.GroupId)
							So(permQuery.Result[0].UserId, ShouldEqual, ac3.Id)
						})
					})
				})
			})
		})
	})
}

func testHelperUpdateDashboardAcl(dashboardId int64, items ...models.DashboardAcl) error {
	cmd := models.UpdateDashboardAclCommand{DashboardID: dashboardId}
	for _, i := range items {
		item := i
		item.Created = time.Now()
		item.Updated = time.Now()
		cmd.Items = append(cmd.Items, &item)
	}
	return UpdateDashboardAcl(&cmd)
}
