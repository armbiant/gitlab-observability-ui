package plugins

import "gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/services/provisioning/values"

// pluginsAsConfig is a normalized data object for plugins config data. Any config version should be mappable.
// to this type.
type pluginsAsConfig struct {
	Apps []*appFromConfig
}

type appFromConfig struct {
	GroupID        int64
	GroupName      string
	PluginID       string
	Enabled        bool
	Pinned         bool
	PluginVersion  string
	JSONData       map[string]interface{}
	SecureJSONData map[string]string
}

type appFromConfigV0 struct {
	GroupID        values.Int64Value     `json:"group_id" yaml:"group_id"`
	GroupName      values.StringValue    `json:"group_name" yaml:"group_name"`
	Type           values.StringValue    `json:"type" yaml:"type"`
	Disabled       values.BoolValue      `json:"disabled" yaml:"disabled"`
	JSONData       values.JSONValue      `json:"jsonData" yaml:"jsonData"`
	SecureJSONData values.StringMapValue `json:"secureJsonData" yaml:"secureJsonData"`
}

// pluginsAsConfigV0 is a mapping for zero version configs. This is mapped to its normalised version.
type pluginsAsConfigV0 struct {
	Apps []*appFromConfigV0 `json:"apps" yaml:"apps"`
}

// mapToPluginsFromConfig maps config syntax to a normalized notificationsAsConfig object. Every version
// of the config syntax should have this function.
func (cfg *pluginsAsConfigV0) mapToPluginsFromConfig() *pluginsAsConfig {
	r := &pluginsAsConfig{}
	if cfg == nil {
		return r
	}

	for _, app := range cfg.Apps {
		r.Apps = append(r.Apps, &appFromConfig{
			GroupID:        app.GroupID.Value(),
			GroupName:      app.GroupName.Value(),
			PluginID:       app.Type.Value(),
			Enabled:        !app.Disabled.Value(),
			Pinned:         true,
			JSONData:       app.JSONData.Value(),
			SecureJSONData: app.SecureJSONData.Value(),
		})
	}

	return r
}
