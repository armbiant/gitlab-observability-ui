package utils

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/models"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/services/sqlstore"
)

func TestCheckOrgExists(t *testing.T) {
	Convey("with default org in database", t, func() {
		sqlstore.InitTestDB(t)

		defaultOrg := models.CreateGroupCommand{Name: "Main Group"}
		err := sqlstore.CreateOrg(&defaultOrg)
		So(err, ShouldBeNil)

		Convey("default org exists", func() {
			err := CheckOrgExists(defaultOrg.Result.Id)
			So(err, ShouldBeNil)
		})

		Convey("other org doesn't exist", func() {
			err := CheckOrgExists(defaultOrg.Result.Id + 1)
			So(err, ShouldEqual, models.ErrGroupNotFound)
		})
	})
}
