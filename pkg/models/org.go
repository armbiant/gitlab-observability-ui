package models

import (
	"errors"
	"time"
)

// Typed errors
var (
	ErrGroupNotFound  = errors.New("group not found")
	ErrGroupNameTaken = errors.New("group name is taken")
)

type Grp struct {
	Id      int64
	Version int
	Name    string

	Address1 string
	Address2 string
	City     string
	ZipCode  string
	State    string
	Country  string

	Created time.Time
	Updated time.Time
}

// ---------------------
// COMMANDS

type CreateGroupCommand struct {
	Name string `json:"name" binding:"Required"`
	// optionally specify the id
	Id int64 `json:"id"`

	// initial admin user for account
	UserId int64 `json:"-"`
	Result Grp   `json:"-"`
}

type DeleteGroupCommand struct {
	Id int64
}

type UpdateGroupCommand struct {
	Name    string
	GroupId int64
}

type UpdateGroupAddressCommand struct {
	GroupId int64
	Address
}

type GetGroupByIdQuery struct {
	Id     int64
	Result *Grp
}

type GetGroupByNameQuery struct {
	Name   string
	Result *Grp
}

type SearchGroupsQuery struct {
	Query string
	Name  string
	Limit int
	Page  int
	Ids   []int64

	Result []*GroupDTO
}

type GroupDTO struct {
	Id   int64  `json:"id"`
	Name string `json:"name"`
}

type GroupDetailsDTO struct {
	Id      int64   `json:"id"`
	Name    string  `json:"name"`
	Address Address `json:"address"`
}

type UserGroupDTO struct {
	GroupId int64    `json:"groupId"`
	Name    string   `json:"name"`
	Role    RoleType `json:"role"`
}
