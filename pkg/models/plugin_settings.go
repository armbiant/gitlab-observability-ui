package models

import (
	"errors"
	"time"

	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/components/securejsondata"
)

var (
	ErrPluginSettingNotFound = errors.New("plugin setting not found")
)

type PluginSetting struct {
	Id             int64
	PluginId       string
	GroupId        int64
	Enabled        bool
	Pinned         bool
	JsonData       map[string]interface{}
	SecureJsonData securejsondata.SecureJsonData
	PluginVersion  string

	Created time.Time
	Updated time.Time
}

// ----------------------
// COMMANDS

// Also acts as api DTO
type UpdatePluginSettingCmd struct {
	Enabled        bool                   `json:"enabled"`
	Pinned         bool                   `json:"pinned"`
	JsonData       map[string]interface{} `json:"jsonData"`
	SecureJsonData map[string]string      `json:"secureJsonData"`
	PluginVersion  string                 `json:"version"`

	PluginId string `json:"-"`
	GroupId  int64  `json:"-"`
}

// specific command, will only update version
type UpdatePluginSettingVersionCmd struct {
	PluginVersion string
	PluginId      string `json:"-"`
	GroupId       int64  `json:"-"`
}

func (cmd *UpdatePluginSettingCmd) GetEncryptedJsonData() securejsondata.SecureJsonData {
	return securejsondata.GetEncryptedJsonData(cmd.SecureJsonData)
}

// ---------------------
// QUERIES
type GetPluginSettingsQuery struct {
	GroupId int64
	Result  []*PluginSettingInfoDTO
}

type PluginSettingInfoDTO struct {
	GroupId       int64
	PluginId      string
	Enabled       bool
	Pinned        bool
	PluginVersion string
}

type GetPluginSettingByIdQuery struct {
	PluginId string
	GroupId  int64
	Result   *PluginSetting
}

type PluginStateChangedEvent struct {
	PluginId string
	GroupId  int64
	Enabled  bool
}
