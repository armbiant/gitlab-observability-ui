package middleware

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/bus"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/models"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/setting"
	"gopkg.in/macaron.v1"
)

// OrgRedirect changes org and redirects users if the
// querystring `orgId` doesn't match the active org.
func OrgRedirect(cfg *setting.Cfg) macaron.Handler {
	return func(res http.ResponseWriter, req *http.Request, c *macaron.Context) {
		groupIdValue := req.URL.Query().Get("groupId")
		groupId, err := strconv.ParseInt(groupIdValue, 10, 64)

		if err != nil || groupId == 0 {
			return
		}

		ctx, ok := c.Data["ctx"].(*models.ReqContext)
		if !ok || !ctx.IsSignedIn {
			return
		}

		if groupId == ctx.GroupId {
			return
		}

		cmd := models.SetUsingOrgCommand{UserId: ctx.UserId, GroupId: groupId}
		if err := bus.Dispatch(&cmd); err != nil {
			if ctx.IsApiRequest() {
				ctx.JsonApiErr(404, "Not found", nil)
			} else {
				ctx.Error(404, "Not found")
			}

			return
		}

		newURL := fmt.Sprintf("%s%s?%s", cfg.AppURL, strings.TrimPrefix(c.Req.URL.Path, "/"), c.Req.URL.Query().Encode())
		c.Redirect(newURL, 302)
	}
}
