package middleware

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/bus"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/models"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/services/auth"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/services/quota"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/setting"
	macaron "gopkg.in/macaron.v1"
)

func TestMiddlewareQuota(t *testing.T) {
	t.Run("With user not logged in", func(t *testing.T) {
		middlewareScenario(t, "and global quota not reached", func(t *testing.T, sc *scenarioContext) {
			bus.AddHandler("globalQuota", func(query *models.GetGlobalQuotaByTargetQuery) error {
				query.Result = &models.GlobalQuotaDTO{
					Target: query.Target,
					Limit:  query.Default,
					Used:   4,
				}
				return nil
			})

			quotaHandler := getQuotaHandler(sc, "user")

			sc.m.Get("/user", quotaHandler, sc.defaultHandler)
			sc.fakeReq("GET", "/user").exec()
			assert.Equal(t, 200, sc.resp.Code)
		}, configure)

		middlewareScenario(t, "and global quota reached", func(t *testing.T, sc *scenarioContext) {
			bus.AddHandler("globalQuota", func(query *models.GetGlobalQuotaByTargetQuery) error {
				query.Result = &models.GlobalQuotaDTO{
					Target: query.Target,
					Limit:  query.Default,
					Used:   4,
				}
				return nil
			})

			quotaHandler := getQuotaHandler(sc, "user")
			sc.m.Get("/user", quotaHandler, sc.defaultHandler)
			sc.fakeReq("GET", "/user").exec()
			assert.Equal(t, 403, sc.resp.Code)
		}, func(cfg *setting.Cfg) {
			configure(cfg)

			cfg.Quota.Global.User = 4
		})

		middlewareScenario(t, "and global session quota not reached", func(t *testing.T, sc *scenarioContext) {
			bus.AddHandler("globalQuota", func(query *models.GetGlobalQuotaByTargetQuery) error {
				query.Result = &models.GlobalQuotaDTO{
					Target: query.Target,
					Limit:  query.Default,
					Used:   4,
				}
				return nil
			})

			quotaHandler := getQuotaHandler(sc, "session")
			sc.m.Get("/user", quotaHandler, sc.defaultHandler)
			sc.fakeReq("GET", "/user").exec()
			assert.Equal(t, 200, sc.resp.Code)
		}, func(cfg *setting.Cfg) {
			configure(cfg)

			cfg.Quota.Global.Session = 10
		})

		middlewareScenario(t, "and global session quota reached", func(t *testing.T, sc *scenarioContext) {
			quotaHandler := getQuotaHandler(sc, "session")
			sc.m.Get("/user", quotaHandler, sc.defaultHandler)
			sc.fakeReq("GET", "/user").exec()
			assert.Equal(t, 403, sc.resp.Code)
		}, func(cfg *setting.Cfg) {
			configure(cfg)

			cfg.Quota.Global.Session = 1
		})
	})

	t.Run("with user logged in", func(t *testing.T) {
		const quotaUsed = 4

		setUp := func(sc *scenarioContext) {
			sc.withTokenSessionCookie("token")
			bus.AddHandler("test", func(query *models.GetSignedInUserQuery) error {
				query.Result = &models.SignedInUser{GroupId: 2, UserId: 12}
				return nil
			})

			sc.userAuthTokenService.LookupTokenProvider = func(ctx context.Context, unhashedToken string) (*models.UserToken, error) {
				return &models.UserToken{
					UserId:        12,
					UnhashedToken: "",
				}, nil
			}

			bus.AddHandler("globalQuota", func(query *models.GetGlobalQuotaByTargetQuery) error {
				query.Result = &models.GlobalQuotaDTO{
					Target: query.Target,
					Limit:  query.Default,
					Used:   quotaUsed,
				}
				return nil
			})

			bus.AddHandler("userQuota", func(query *models.GetUserQuotaByTargetQuery) error {
				query.Result = &models.UserQuotaDTO{
					Target: query.Target,
					Limit:  query.Default,
					Used:   quotaUsed,
				}
				return nil
			})

			bus.AddHandler("groupQuota", func(query *models.GetOrgQuotaByTargetQuery) error {
				query.Result = &models.OrgQuotaDTO{
					Target: query.Target,
					Limit:  query.Default,
					Used:   quotaUsed,
				}
				return nil
			})
		}

		middlewareScenario(t, "global datasource quota reached", func(t *testing.T, sc *scenarioContext) {
			setUp(sc)

			quotaHandler := getQuotaHandler(sc, "data_source")
			sc.m.Get("/ds", quotaHandler, sc.defaultHandler)
			sc.fakeReq("GET", "/ds").exec()
			assert.Equal(t, 403, sc.resp.Code)
		}, func(cfg *setting.Cfg) {
			configure(cfg)

			cfg.Quota.Global.DataSource = quotaUsed
		})

		middlewareScenario(t, "user Org quota not reached", func(t *testing.T, sc *scenarioContext) {
			setUp(sc)

			quotaHandler := getQuotaHandler(sc, "grp")

			sc.m.Get("/group", quotaHandler, sc.defaultHandler)
			sc.fakeReq("GET", "/group").exec()
			assert.Equal(t, 200, sc.resp.Code)
		}, func(cfg *setting.Cfg) {
			configure(cfg)

			cfg.Quota.User.Org = quotaUsed + 1
		})

		middlewareScenario(t, "user Org quota reached", func(t *testing.T, sc *scenarioContext) {
			setUp(sc)

			quotaHandler := getQuotaHandler(sc, "grp")
			sc.m.Get("/group", quotaHandler, sc.defaultHandler)
			sc.fakeReq("GET", "/group").exec()
			assert.Equal(t, 403, sc.resp.Code)
		}, func(cfg *setting.Cfg) {
			configure(cfg)

			cfg.Quota.User.Org = quotaUsed
		})

		middlewareScenario(t, "group dashboard quota not reached", func(t *testing.T, sc *scenarioContext) {
			setUp(sc)

			quotaHandler := getQuotaHandler(sc, "dashboard")
			sc.m.Get("/dashboard", quotaHandler, sc.defaultHandler)
			sc.fakeReq("GET", "/dashboard").exec()
			assert.Equal(t, 200, sc.resp.Code)
		}, func(cfg *setting.Cfg) {
			configure(cfg)

			cfg.Quota.Org.Dashboard = quotaUsed + 1
		})

		middlewareScenario(t, "group dashboard quota reached", func(t *testing.T, sc *scenarioContext) {
			setUp(sc)

			quotaHandler := getQuotaHandler(sc, "dashboard")
			sc.m.Get("/dashboard", quotaHandler, sc.defaultHandler)
			sc.fakeReq("GET", "/dashboard").exec()
			assert.Equal(t, 403, sc.resp.Code)
		}, func(cfg *setting.Cfg) {
			configure(cfg)

			cfg.Quota.Org.Dashboard = quotaUsed
		})

		middlewareScenario(t, "group dashboard quota reached, but quotas disabled", func(t *testing.T, sc *scenarioContext) {
			setUp(sc)

			quotaHandler := getQuotaHandler(sc, "dashboard")
			sc.m.Get("/dashboard", quotaHandler, sc.defaultHandler)
			sc.fakeReq("GET", "/dashboard").exec()
			assert.Equal(t, 200, sc.resp.Code)
		}, func(cfg *setting.Cfg) {
			configure(cfg)

			cfg.Quota.Org.Dashboard = quotaUsed
			cfg.Quota.Enabled = false
		})
	})
}

func getQuotaHandler(sc *scenarioContext, target string) macaron.Handler {
	fakeAuthTokenService := auth.NewFakeUserAuthTokenService()
	qs := &quota.QuotaService{
		AuthTokenService: fakeAuthTokenService,
		Cfg:              sc.cfg,
	}

	return Quota(qs)(target)
}

func configure(cfg *setting.Cfg) {
	cfg.AnonymousEnabled = false
	cfg.Quota = setting.QuotaSettings{
		Enabled: true,
		Org: &setting.OrgQuota{
			User:       5,
			Dashboard:  5,
			DataSource: 5,
			ApiKey:     5,
		},
		User: &setting.UserQuota{
			Org: 5,
		},
		Global: &setting.GlobalQuota{
			Org:        5,
			User:       5,
			Dashboard:  5,
			DataSource: 5,
			ApiKey:     5,
			Session:    5,
		},
	}
}
