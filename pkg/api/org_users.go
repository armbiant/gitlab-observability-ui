package api

import (
	"errors"

	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/api/dtos"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/api/response"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/bus"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/models"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/util"
)

// POST /api/group/users
func AddOrgUserToCurrentOrg(c *models.ReqContext, cmd models.AddGroupUserCommand) response.Response {
	cmd.GroupId = c.GroupId
	return addOrgUserHelper(cmd)
}

// POST /api/groups/:orgId/users
func AddOrgUser(c *models.ReqContext, cmd models.AddGroupUserCommand) response.Response {
	cmd.GroupId = c.ParamsInt64(":orgId")
	return addOrgUserHelper(cmd)
}

func addOrgUserHelper(cmd models.AddGroupUserCommand) response.Response {
	if !cmd.Role.IsValid() {
		return response.Error(400, "Invalid role specified", nil)
	}

	userQuery := models.GetUserByLoginQuery{LoginOrEmail: cmd.LoginOrEmail}
	err := bus.Dispatch(&userQuery)
	if err != nil {
		return response.Error(404, "User not found", nil)
	}

	userToAdd := userQuery.Result

	cmd.UserId = userToAdd.Id

	if err := bus.Dispatch(&cmd); err != nil {
		if errors.Is(err, models.ErrGroupUserAlreadyAdded) {
			return response.JSON(409, util.DynMap{
				"message": "User is already member of this group",
				"userId":  cmd.UserId,
			})
		}
		return response.Error(500, "Could not add user to group", err)
	}

	return response.JSON(200, util.DynMap{
		"message": "User added to group",
		"userId":  cmd.UserId,
	})
}

// GET /api/group/users
func (hs *HTTPServer) GetOrgUsersForCurrentOrg(c *models.ReqContext) response.Response {
	result, err := hs.getOrgUsersHelper(&models.GetGroupUsersQuery{
		GroupId: c.GroupId,
		Query:   c.Query("query"),
		Limit:   c.QueryInt("limit"),
	}, c.SignedInUser)

	if err != nil {
		return response.Error(500, "Failed to get users for current group", err)
	}

	return response.JSON(200, result)
}

// GET /api/group/users/lookup
func (hs *HTTPServer) GetOrgUsersForCurrentOrgLookup(c *models.ReqContext) response.Response {
	isAdmin, err := isOrgAdminFolderAdminOrTeamAdmin(c)
	if err != nil {
		return response.Error(500, "Failed to get users for current group", err)
	}

	if !isAdmin {
		return response.Error(403, "Permission denied", nil)
	}

	orgUsers, err := hs.getOrgUsersHelper(&models.GetGroupUsersQuery{
		GroupId: c.GroupId,
		Query:   c.Query("query"),
		Limit:   c.QueryInt("limit"),
	}, c.SignedInUser)

	if err != nil {
		return response.Error(500, "Failed to get users for current group", err)
	}

	result := make([]*dtos.UserLookupDTO, 0)

	for _, u := range orgUsers {
		result = append(result, &dtos.UserLookupDTO{
			UserID:    u.UserId,
			Login:     u.Login,
			AvatarURL: u.AvatarUrl,
		})
	}

	return response.JSON(200, result)
}

func isOrgAdminFolderAdminOrTeamAdmin(c *models.ReqContext) (bool, error) {
	if c.OrgRole == models.ROLE_ADMIN {
		return true, nil
	}

	hasAdminPermissionInFoldersQuery := models.HasAdminPermissionInFoldersQuery{SignedInUser: c.SignedInUser}
	if err := bus.Dispatch(&hasAdminPermissionInFoldersQuery); err != nil {
		return false, err
	}

	if hasAdminPermissionInFoldersQuery.Result {
		return true, nil
	}

	isAdminOfTeamsQuery := models.IsAdminOfTeamsQuery{SignedInUser: c.SignedInUser}
	if err := bus.Dispatch(&isAdminOfTeamsQuery); err != nil {
		return false, err
	}

	return isAdminOfTeamsQuery.Result, nil
}

// GET /api/groups/:orgId/users
func (hs *HTTPServer) GetOrgUsers(c *models.ReqContext) response.Response {
	result, err := hs.getOrgUsersHelper(&models.GetGroupUsersQuery{
		GroupId: c.ParamsInt64(":orgId"),
		Query:   "",
		Limit:   0,
	}, c.SignedInUser)

	if err != nil {
		return response.Error(500, "Failed to get users for group", err)
	}

	return response.JSON(200, result)
}

func (hs *HTTPServer) getOrgUsersHelper(query *models.GetGroupUsersQuery, signedInUser *models.SignedInUser) ([]*models.GroupUserDTO, error) {
	if err := bus.Dispatch(query); err != nil {
		return nil, err
	}

	filteredUsers := make([]*models.GroupUserDTO, 0, len(query.Result))
	for _, user := range query.Result {
		if dtos.IsHiddenUser(user.Login, signedInUser, hs.Cfg) {
			continue
		}
		user.AvatarUrl = dtos.GetGravatarUrl(user.Email)

		filteredUsers = append(filteredUsers, user)
	}

	return filteredUsers, nil
}

// PATCH /api/group/users/:userId
func UpdateOrgUserForCurrentOrg(c *models.ReqContext, cmd models.UpdateGroupUserCommand) response.Response {
	cmd.GroupId = c.GroupId
	cmd.UserId = c.ParamsInt64(":userId")
	return updateOrgUserHelper(cmd)
}

// PATCH /api/groups/:orgId/users/:userId
func UpdateOrgUser(c *models.ReqContext, cmd models.UpdateGroupUserCommand) response.Response {
	cmd.GroupId = c.ParamsInt64(":orgId")
	cmd.UserId = c.ParamsInt64(":userId")
	return updateOrgUserHelper(cmd)
}

func updateOrgUserHelper(cmd models.UpdateGroupUserCommand) response.Response {
	if !cmd.Role.IsValid() {
		return response.Error(400, "Invalid role specified", nil)
	}

	if err := bus.Dispatch(&cmd); err != nil {
		if errors.Is(err, models.ErrLastGroupAdmin) {
			return response.Error(400, "Cannot change role so that there is no group admin left", nil)
		}
		return response.Error(500, "Failed update group user", err)
	}

	return response.Success("Organization user updated")
}

// DELETE /api/group/users/:userId
func RemoveOrgUserForCurrentOrg(c *models.ReqContext) response.Response {
	return removeOrgUserHelper(&models.RemoveGroupUserCommand{
		UserId:                   c.ParamsInt64(":userId"),
		GroupId:                  c.GroupId,
		ShouldDeleteOrphanedUser: true,
	})
}

// DELETE /api/groups/:orgId/users/:userId
func RemoveOrgUser(c *models.ReqContext) response.Response {
	return removeOrgUserHelper(&models.RemoveGroupUserCommand{
		UserId:  c.ParamsInt64(":userId"),
		GroupId: c.ParamsInt64(":orgId"),
	})
}

func removeOrgUserHelper(cmd *models.RemoveGroupUserCommand) response.Response {
	if err := bus.Dispatch(cmd); err != nil {
		if errors.Is(err, models.ErrLastGroupAdmin) {
			return response.Error(400, "Cannot remove last group admin", nil)
		}
		return response.Error(500, "Failed to remove user from group", err)
	}

	if cmd.UserWasDeleted {
		return response.Success("User deleted")
	}

	return response.Success("User removed from group")
}
