package api

import (
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/api/response"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/bus"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/models"
	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/setting"
)

func GetOrgQuotas(c *models.ReqContext) response.Response {
	if !setting.Quota.Enabled {
		return response.Error(404, "Quotas not enabled", nil)
	}
	query := models.GetOrgQuotasQuery{GroupId: c.ParamsInt64(":groupId")}

	if err := bus.Dispatch(&query); err != nil {
		return response.Error(500, "Failed to get group quotas", err)
	}

	return response.JSON(200, query.Result)
}

func UpdateOrgQuota(c *models.ReqContext, cmd models.UpdateOrgQuotaCmd) response.Response {
	if !setting.Quota.Enabled {
		return response.Error(404, "Quotas not enabled", nil)
	}
	cmd.GroupId = c.ParamsInt64(":groupId")
	cmd.Target = c.Params(":target")

	if _, ok := setting.Quota.Org.ToMap()[cmd.Target]; !ok {
		return response.Error(404, "Invalid quota target", nil)
	}

	if err := bus.Dispatch(&cmd); err != nil {
		return response.Error(500, "Failed to update group quotas", err)
	}
	return response.Success("Group quota updated")
}

func GetUserQuotas(c *models.ReqContext) response.Response {
	if !setting.Quota.Enabled {
		return response.Error(404, "Quotas not enabled", nil)
	}
	query := models.GetUserQuotasQuery{UserId: c.ParamsInt64(":id")}

	if err := bus.Dispatch(&query); err != nil {
		return response.Error(500, "Failed to get group quotas", err)
	}

	return response.JSON(200, query.Result)
}

func UpdateUserQuota(c *models.ReqContext, cmd models.UpdateUserQuotaCmd) response.Response {
	if !setting.Quota.Enabled {
		return response.Error(404, "Quotas not enabled", nil)
	}
	cmd.UserId = c.ParamsInt64(":id")
	cmd.Target = c.Params(":target")

	if _, ok := setting.Quota.User.ToMap()[cmd.Target]; !ok {
		return response.Error(404, "Invalid quota target", nil)
	}

	if err := bus.Dispatch(&cmd); err != nil {
		return response.Error(500, "Failed to update group quotas", err)
	}
	return response.Success("Group quota updated")
}
