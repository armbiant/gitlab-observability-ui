package imguploader

import (
	"context"
	"path"
	"path/filepath"

	"gitlab.com/gitlab-org/opstrace/opstrace-ui/pkg/setting"
)

type LocalUploader struct {
}

func (u *LocalUploader) Upload(ctx context.Context, imageOnDiskPath string) (string, error) {
	filename := filepath.Base(imageOnDiskPath)
	image_url := setting.ToAbsUrl(path.Join("public/img/attachments", filename))
	return image_url, nil
}

func NewLocalImageUploader() (*LocalUploader, error) {
	return &LocalUploader{}, nil
}
