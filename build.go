//go:build ignore
// +build ignore

package main

import (
	"archive/tar"
	"bytes"
	"compress/gzip"
	"crypto/md5"
	"crypto/sha256"
	"encoding/json"
	"flag"
	"fmt"
	"go/build"
	"io"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"
	"time"
)

const (
	linux = "linux"
)

var (
	//versionRe = regexp.MustCompile(`-[0-9]{1,3}-g[0-9a-f]{5,10}`)
	goarch          string
	dockerarch      string
	goos            string
	gocc            string
	cgo             bool
	libc            string
	version         string = "v1"
	buildTags       []string
	race            bool
	workingDir      string
	serverBinary    string   = "grafana-server"
	cliBinary       string   = "grafana-cli"
	binaries        []string = []string{serverBinary, cliBinary}
	isDev           bool     = false
	printGenVersion bool     = false
)

func main() {
	log.SetOutput(os.Stdout)
	log.SetFlags(0)

	var buildTagsRaw string

	flag.StringVar(&goarch, "goarch", runtime.GOARCH, "GOARCH")
	flag.StringVar(&goos, "goos", runtime.GOOS, "GOOS")
	flag.StringVar(&gocc, "cc", "", "CC")
	flag.StringVar(&libc, "libc", "", "LIBC")
	flag.StringVar(&buildTagsRaw, "build-tags", "", "Sets custom build tags")
	flag.BoolVar(&cgo, "cgo-enabled", cgo, "Enable cgo")
	flag.BoolVar(&race, "race", race, "Use race detector")
	flag.BoolVar(&isDev, "dev", isDev, "optimal for development, skips certain steps")
	flag.BoolVar(&printGenVersion, "gen-version", printGenVersion, "generate Grafana version and output (default: false)")
	flag.Parse()

	readVersionFromPackageJson()

	if printGenVersion {
		printGeneratedVersion()
		return
	}

	if len(buildTagsRaw) > 0 {
		buildTags = strings.Split(buildTagsRaw, ",")
	}

	log.Printf("Version: %s\n", version)

	if flag.NArg() == 0 {
		log.Println("Usage: go run build.go build")
		return
	}

	workingDir, _ = os.Getwd()

	for _, cmd := range flag.Args() {
		switch cmd {
		case "setup":
			setup()

		case "build-server":
			clean()
			doBuild(serverBinary, "./pkg/cmd/grafana-server", buildTags)

		case "build-cli":
			clean()
			doBuild(cliBinary, "./pkg/cmd/grafana-cli", buildTags)

		case "package-only":
			doPackage()

		case "build":
			//clean()
			for _, binary := range binaries {
				doBuild(binary, "./pkg/cmd/"+binary, buildTags)
			}

		case "build-frontend":
			yarn("run", "build")
			yarn("run", "plugins:build-bundled")

		case "sha-dist":
			shaFilesInDist()

		case "clean":
			clean()

		default:
			log.Fatalf("Unknown command %q", cmd)
		}
	}
}

func doPackage() {
	err := os.MkdirAll("dist", 0755)
	if err != nil {
		log.Fatalf("Failed to create directory: %v", err)
	}

	err = os.WriteFile("VERSION", []byte(version), 0644)
	if err != nil {
		log.Fatalf("Failed to create version file: %v", err)
	}
	defer os.Remove("VERSION")

	var destFile string
	if libc != "" {
		destFile = fmt.Sprintf("dist/grafana-%s.linux-%s-%s.tar.gz", version, goarch, libc)
	} else {
		destFile = fmt.Sprintf("dist/grafana-%s.linux-%s.tar.gz", version, goarch)
	}
	archivePrefix := fmt.Sprintf("grafana-%s/", version)
	srcPaths := []string{
		fmt.Sprintf("bin/linux-%s/", goarch),
		"conf/",
		"public/",
		"plugins-bundled/",
		"scripts/*.sh",
		"VERSION",
		"LICENSE",
		"README.md",
		"NOTICE.md",
	}

	createReleaseTarball(srcPaths, archivePrefix, destFile, version)
}

func createReleaseTarball(
	srcPaths []string,
	archivePrefix string,
	destFile string,
	version string,
) {
	log.Printf("Packaging %s in %s", version, destFile)

	srcFiles := findFiles(srcPaths...)

	// Create output file
	out, err := os.Create(destFile)
	if err != nil {
		log.Fatalf("Error creating destination file %s: %v", destFile, err)
	}
	defer out.Close()

	h := md5.New()

	copier := io.MultiWriter(h, out)

	gw := gzip.NewWriter(copier)
	defer gw.Close()
	tw := tar.NewWriter(gw)
	defer tw.Close()

	pathTransformer := func(inPath string) string {
		binPrefix := fmt.Sprintf("bin/linux-%s", goarch)
		if strings.HasPrefix(inPath, binPrefix) {
			inPath = strings.Replace(inPath, binPrefix, "bin", 1)
		}
		inPath = fmt.Sprintf("grafana-%s/%s", version, inPath)
		return inPath
	}

	for _, srcFile := range srcFiles {
		err := addToArchive(tw, srcFile, pathTransformer)
		if err != nil {
			log.Fatalf("unable to add file %s to archive: %v", srcFile, err)
		}
	}

	log.Printf("Release tarball %s created successfully, md5sum: %x", destFile, h.Sum(nil))
}

func findFiles(srcPaths ...string) []string {
	foundFiles := make([]string, 0)

	walkFunc := func(path string, info os.FileInfo, err error) error {
		if err != nil {
			log.Fatalf("error occured when accesing path %s: %v", path, err)
			return nil
		}

		if !info.IsDir() {
			foundFiles = append(foundFiles, path)
		}

		return nil
	}

	globbedPaths := make([]string, 0, len(srcPaths))
	for _, srcPath := range srcPaths {
		if !strings.Contains(srcPath, "*") {
			globbedPaths = append(globbedPaths, srcPath)
			continue
		}

		matches, err := filepath.Glob(srcPath)
		if err != nil {
			log.Fatalf("unable to glob path %s: %v", srcPath, err)
		}
		if len(matches) == 0 {
			log.Printf("globbing %s did not yield any new paths", srcPath)
			continue
		}

		globbedPaths = append(globbedPaths, matches...)
	}

	for _, globbedPath := range globbedPaths {
		err := filepath.Walk(globbedPath, walkFunc)

		if err != nil {
			log.Fatalf("error occured when traversing path %s: %v", globbedPath, err)
		}
	}

	return foundFiles
}

func addToArchive(tw *tar.Writer, filename string, pathTransformer func(string) string) error {
	file, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer file.Close()

	info, err := file.Stat()
	if err != nil {
		return err
	}

	header, err := tar.FileInfoHeader(info, info.Name())
	if err != nil {
		return err
	}
	header.Uid = 0
	header.Gid = 0
	header.Uname = "root"
	header.Gname = "root"
	header.Name = pathTransformer(filename)

	err = tw.WriteHeader(header)
	if err != nil {
		return err
	}

	_, err = io.Copy(tw, file)
	if err != nil {
		return err
	}

	return nil
}

func readVersionFromPackageJson() {
	reader, err := os.Open("package.json")
	if err != nil {
		log.Fatalf("Failed to open package.json: %v", err)
		return
	}
	defer reader.Close()

	jsonObj := map[string]interface{}{}
	jsonParser := json.NewDecoder(reader)

	if err := jsonParser.Decode(&jsonObj); err != nil {
		log.Fatalf("Failed to decode package.json: %v", err)
	}

	version = jsonObj["version"].(string)
}

func yarn(params ...string) {
	runPrint(`yarn`, params...)
}

func setup() {
	args := []string{"install", "-v"}
	args = append(args, "./pkg/cmd/grafana-server")
	runPrint("go", args...)
}

func printGeneratedVersion() {
	fmt.Print(version)
}

func test(pkg string) {
	setBuildEnv()
	args := []string{"test", "-short", "-timeout", "60s"}
	args = append(args, pkg)
	runPrint("go", args...)
}

func doBuild(binaryName, pkg string, tags []string) {
	libcPart := ""
	if libc != "" {
		libcPart = fmt.Sprintf("-%s", libc)
	}
	binary := fmt.Sprintf("./bin/%s-%s%s/%s", goos, goarch, libcPart, binaryName)
	if isDev {
		//don't include os/arch/libc in output path in dev environment
		binary = fmt.Sprintf("./bin/%s", binaryName)
	}

	if !isDev {
		rmr(binary, binary+".md5")
	}
	args := []string{"build", "-ldflags", ldflags()}
	if len(tags) > 0 {
		args = append(args, "-tags", strings.Join(tags, ","))
	}
	if race {
		args = append(args, "-race")
	}

	args = append(args, "-o", binary)
	args = append(args, pkg)

	if !isDev {
		setBuildEnv()
		runPrint("go", "version")
		libcPart := ""
		if libc != "" {
			libcPart = fmt.Sprintf("/%s", libc)
		}
		fmt.Printf("Targeting %s/%s%s\n", goos, goarch, libcPart)
	}

	runPrint("go", args...)

	if !isDev {
		// Create an md5 checksum of the binary, to be included in the archive for
		// automatic upgrades.
		err := md5File(binary)
		if err != nil {
			log.Fatalf("failed to calculate md5sum: %v", err)
		}
	}
}

func ldflags() string {
	var b bytes.Buffer
	b.WriteString("-w")
	b.WriteString(fmt.Sprintf(" -X main.version=%s", version))
	b.WriteString(fmt.Sprintf(" -X main.commit=%s", getGitSha()))
	b.WriteString(fmt.Sprintf(" -X main.buildstamp=%d", buildStamp()))
	b.WriteString(fmt.Sprintf(" -X main.buildBranch=%s", getGitBranch()))
	if v := os.Getenv("LDFLAGS"); v != "" {
		b.WriteString(fmt.Sprintf(" -extldflags \"%s\"", v))
	}
	return b.String()
}

func rmr(paths ...string) {
	for _, path := range paths {
		log.Println("rm -r", path)
		os.RemoveAll(path)
	}
}

func clean() {
	if isDev {
		return
	}

	rmr("dist")
	rmr("tmp")
	rmr(filepath.Join(build.Default.GOPATH, fmt.Sprintf("pkg/%s_%s/github.com/grafana", goos, goarch)))
}

func setBuildEnv() {
	os.Setenv("GOOS", goos)
	if goarch != "amd64" || goos != linux {
		// needed for all other archs
		cgo = true
	}
	if strings.HasPrefix(goarch, "armv") {
		os.Setenv("GOARCH", "arm")
		os.Setenv("GOARM", goarch[4:])
	} else {
		os.Setenv("GOARCH", goarch)
	}
	if goarch == "386" {
		os.Setenv("GO386", "387")
	}
	if cgo {
		os.Setenv("CGO_ENABLED", "1")
	}
	if gocc != "" {
		os.Setenv("CC", gocc)
	}
}

func getGitBranch() string {
	v, err := runError("git", "rev-parse", "--abbrev-ref", "HEAD")
	if err != nil {
		return "master"
	}
	return string(v)
}

func getGitSha() string {
	v, err := runError("git", "rev-parse", "--short", "HEAD")
	if err != nil {
		return "unknown-dev"
	}
	return string(v)
}

func buildStamp() int64 {
	// use SOURCE_DATE_EPOCH if set.
	if s, _ := strconv.ParseInt(os.Getenv("SOURCE_DATE_EPOCH"), 10, 64); s > 0 {
		return s
	}

	bs, err := runError("git", "show", "-s", "--format=%ct")
	if err != nil {
		return time.Now().Unix()
	}
	s, _ := strconv.ParseInt(string(bs), 10, 64)
	return s
}

func runError(cmd string, args ...string) ([]byte, error) {
	ecmd := exec.Command(cmd, args...)
	bs, err := ecmd.CombinedOutput()
	if err != nil {
		return nil, err
	}

	return bytes.TrimSpace(bs), nil
}

func runPrint(cmd string, args ...string) {
	log.Println(cmd, strings.Join(args, " "))
	ecmd := exec.Command(cmd, args...)
	ecmd.Env = append(os.Environ(), "GO111MODULE=on")
	ecmd.Stdout = os.Stdout
	ecmd.Stderr = os.Stderr
	err := ecmd.Run()
	if err != nil {
		log.Fatalf("failed to execute command: %v", err)
	}
}

func md5File(file string) error {
	fd, err := os.Open(file)
	if err != nil {
		return err
	}
	defer fd.Close()

	h := md5.New()
	_, err = io.Copy(h, fd)
	if err != nil {
		return err
	}

	out, err := os.Create(file + ".md5")
	if err != nil {
		return err
	}

	_, err = fmt.Fprintf(out, "%x\n", h.Sum(nil))
	if err != nil {
		return err
	}

	return out.Close()
}

func shaFilesInDist() {
	filepath.Walk("./dist", func(path string, f os.FileInfo, err error) error {
		if path == "./dist" {
			return nil
		}

		if !strings.Contains(path, ".sha256") {
			err := shaFile(path)
			if err != nil {
				log.Printf("Failed to create sha file. error: %v\n", err)
			}
		}
		return nil
	})
}

func shaFile(file string) error {
	fd, err := os.Open(file)
	if err != nil {
		return err
	}
	defer fd.Close()

	h := sha256.New()
	_, err = io.Copy(h, fd)
	if err != nil {
		return err
	}

	out, err := os.Create(file + ".sha256")
	if err != nil {
		return err
	}

	_, err = fmt.Fprintf(out, "%x\n", h.Sum(nil))
	if err != nil {
		return err
	}

	return out.Close()
}
