export enum MESSAGE_EVENT_TYPE {
  GOUI_LOADED = 'GOUI_LOADED',
  GOUI_ROUTE_UPDATE = 'GOUI_ROUTE_UPDATE',
}

export interface GoUiLoadedEvent {
  type: MESSAGE_EVENT_TYPE.GOUI_LOADED;
}

export interface GoUiRouteUpdateEvent {
  type: MESSAGE_EVENT_TYPE.GOUI_ROUTE_UPDATE;
  payload: { url: string };
}

export type PostMessageData = GoUiLoadedEvent | GoUiRouteUpdateEvent;
