/**
 * isEmbedded
 * method checks if it is embeded into GitLab or not
 * @returns boolean
 */
export function isEmbedded() {
  try {
    return window.self !== window.top;
  } catch (e) {
    return true;
  }
}
