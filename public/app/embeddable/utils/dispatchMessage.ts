import { logError, config } from '@grafana/runtime';
import { PostMessageData } from '../types';
import { isEmbedded } from './isEmbedded';

export const dispatchMessage = (data: PostMessageData) => {
  if (isEmbedded() && config.embeddingParentUrl) {
    try {
      window.parent?.postMessage(data, config.embeddingParentUrl);
    } catch (e) {
      logError(e);
    }
  }
};
