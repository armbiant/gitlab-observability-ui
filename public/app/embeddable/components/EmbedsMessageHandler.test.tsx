import React from 'react';
import { mount } from 'enzyme';
import { asMock } from 'app/core/utils/testUtils';
import EmbedsMessageHandler from './EmbedsMessageHandler';

import { useSelector } from 'react-redux';
import { isEmbedded } from '../utils/isEmbedded';
import { config } from '@grafana/runtime';
import { MESSAGE_EVENT_TYPE } from '../types';

jest.mock('react-redux');
jest.mock('../utils/isEmbedded');
jest.mock('@grafana/runtime');

const mountComponent = () => {
  return mount(<EmbedsMessageHandler />);
};

const dispatchOnLoad = () => {
  const event = new Event('DOMContentLoaded');
  window.dispatchEvent(event);
};

describe('Render', () => {
  beforeEach(() => {
    window.postMessage = jest.fn();
  });

  it('should not postMessage when GOUI is not embedded', () => {
    asMock(isEmbedded).mockReturnValueOnce(false);

    mountComponent();

    expect(window.postMessage).toHaveBeenCalledTimes(0);
  });

  it('should not postMessage DOMContentLoaded event when goui is not embeded', () => {
    asMock(isEmbedded).mockReturnValueOnce(false);

    mountComponent();
    dispatchOnLoad();

    expect(window.postMessage).toHaveBeenCalledTimes(0);
  });

  describe('when GOUI is embedded', () => {
    beforeEach(() => {
      asMock(isEmbedded).mockReturnValueOnce(true);
      asMock(useSelector).mockReturnValue('https://test.com');
      config.embeddingParentUrl = 'https://www.test-origin.com/';
    });

    it('should postMessage to parent with current url', () => {
      mountComponent();

      expect(window.postMessage).toHaveBeenCalledTimes(1);
      expect(window.postMessage).toHaveBeenCalledWith(
        { type: MESSAGE_EVENT_TYPE.GOUI_ROUTE_UPDATE, payload: { url: 'https://test.com' } },
        'https://www.test-origin.com/'
      );
    });

    it('should postMessage to parent on load', () => {
      asMock(isEmbedded).mockReturnValue(true);

      mountComponent();
      dispatchOnLoad();

      expect(window.postMessage).toHaveBeenCalledWith(
        { type: MESSAGE_EVENT_TYPE.GOUI_LOADED },
        'https://www.test-origin.com/'
      );
    });

    describe('when targetOrigin if missing from the config', () => {
      beforeEach(() => {
        config.embeddingParentUrl = '';
        mountComponent();
      });

      it('should not post a route change message to parent', () => {
        expect(window.postMessage).toHaveBeenCalledTimes(0);
      });

      it('should not post a DOMContentLoaded event message to parent', () => {
        dispatchOnLoad();
        expect(window.postMessage).toHaveBeenCalledTimes(0);
      });
    });
  });
});
