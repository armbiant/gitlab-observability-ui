import React from 'react';
import { useLoadEventListener } from '../hooks/useLoadEventListener';
import { useRouteChangeListener } from '../hooks/useRouteChangeListener';

const EmbedsMessageHandler = () => {
  useRouteChangeListener();
  useLoadEventListener();

  return <></>;
};

export default EmbedsMessageHandler;
