import { buildCategories } from './buildCategories';
import { getMockPlugin } from '../../plugins/__mocks__/pluginMocks';
import { DataSourcePluginMeta } from '@grafana/data';

const plugins: DataSourcePluginMeta[] = [
  {
    ...getMockPlugin({ id: 'prometheus', name: 'Prometheus' }),
    category: 'tsdb',
  },
  {
    ...getMockPlugin({ id: 'elasticsearch', name: 'Elasticsearch' }),
    category: 'logging',
  },
  {
    ...getMockPlugin({ id: 'loki', name: 'Loki' }),
    category: 'logging',
  },
  {
    ...getMockPlugin({ id: 'azure', name: 'Azure' }),
    category: 'cloud',
  },
];

describe('buildCategories', () => {
  const categories = buildCategories(plugins);

  it('should group plugins into categories and remove empty categories', () => {
    expect(categories.length).toBe(3);
    expect(categories[0].title).toBe('Time series databases');
    expect(categories[0].plugins.length).toBe(1);
    expect(categories[1].title).toBe('Logging & document databases');
  });

  it('should sort plugins according to name', () => {
    expect(categories[1].plugins[0].id).toBe('elasticsearch');
  });
});
