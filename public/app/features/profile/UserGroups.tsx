import React, { PureComponent } from 'react';
import { UserDTO, UserGroup } from 'app/types';
import { LoadingPlaceholder, Button } from '@grafana/ui';

export interface Props {
  user: UserDTO;
  groups: UserGroup[];
  isLoading: boolean;
  loadGroups: () => void;
  setUserGroup: (org: UserGroup) => void;
}

export class UserGroups extends PureComponent<Props> {
  componentDidMount() {
    this.props.loadGroups();
  }

  render() {
    const { isLoading, groups, user } = this.props;

    if (isLoading) {
      return <LoadingPlaceholder text="Loading groups..." />;
    }

    return (
      <>
        {groups.length > 0 && (
          <>
            <h3 className="page-sub-heading">Groups</h3>
            <div className="gf-form-group">
              <table className="filter-table form-inline">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Role</th>
                    <th />
                  </tr>
                </thead>
                <tbody>
                  {groups.map((group: UserGroup, index) => {
                    return (
                      <tr key={index}>
                        <td>{group.name}</td>
                        <td>{group.role}</td>
                        <td className="text-right">
                          {group.groupId === user.groupId ? (
                            <span className="btn btn-primary btn-small">Current</span>
                          ) : (
                            <Button
                              variant="secondary"
                              size="sm"
                              onClick={() => {
                                this.props.setUserGroup(group);
                              }}
                            >
                              Select
                            </Button>
                          )}
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          </>
        )}
      </>
    );
  }
}

export default UserGroups;
