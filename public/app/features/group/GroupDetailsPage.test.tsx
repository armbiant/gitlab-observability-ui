import React from 'react';
import { shallow } from 'enzyme';
import { NavModel } from '@grafana/data';

import { GroupDetailsPage, Props } from './GroupDetailsPage';
import { Group } from '../../types';
import { mockToolkitActionCreator } from 'test/core/redux/mocks';
import { setGroupName } from './state/reducers';

const setup = (propOverrides?: object) => {
  const props: Props = {
    group: {} as Group,
    navModel: {
      main: {
        text: 'Configuration',
      },
      node: {
        text: 'Group details',
      },
    } as NavModel,
    loadGroup: jest.fn(),
    setGroupName: mockToolkitActionCreator(setGroupName),
    updateGroup: jest.fn(),
  };

  Object.assign(props, propOverrides);

  return shallow(<GroupDetailsPage {...props} />);
};

describe('Render', () => {
  it('should render component', () => {
    const wrapper = setup();

    expect(wrapper).toMatchSnapshot();
  });

  it('should render group and preferences', () => {
    const wrapper = setup({
      group: {
        name: 'Cool group',
        id: 1,
      },
      preferences: {
        homeDashboardId: 1,
        theme: 'Default',
        timezone: 'Default',
      },
    });

    expect(wrapper).toMatchSnapshot();
  });
});
