import { ThunkResult } from 'app/types';
import { getBackendSrv } from '@grafana/runtime';
import { groupLoaded } from './reducers';
import { updateConfigurationSubtitle } from 'app/core/actions';

type GroupDependencies = { getBackendSrv: typeof getBackendSrv };

export function loadGroup(dependencies: GroupDependencies = { getBackendSrv: getBackendSrv }): ThunkResult<any> {
  return async (dispatch) => {
    const groupResponse = await dependencies.getBackendSrv().get('/api/group');
    dispatch(groupLoaded(groupResponse));

    return groupResponse;
  };
}

export function updateGroup(dependencies: GroupDependencies = { getBackendSrv: getBackendSrv }): ThunkResult<any> {
  return async (dispatch, getStore) => {
    const group = getStore().group.group;

    await dependencies.getBackendSrv().put('/api/group', { name: group.name });

    dispatch(updateConfigurationSubtitle(group.name));
    dispatch(loadGroup(dependencies));
  };
}
