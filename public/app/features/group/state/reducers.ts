import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { Group, GroupState } from 'app/types';

export const initialState: GroupState = {
  group: {} as Group,
};

const groupSlice = createSlice({
  name: 'group',
  initialState,
  reducers: {
    groupLoaded: (state, action: PayloadAction<Group>): GroupState => {
      return { ...state, group: action.payload };
    },
    setGroupName: (state, action: PayloadAction<string>): GroupState => {
      return { ...state, group: { ...state.group, name: action.payload } };
    },
  },
});

export const { setGroupName, groupLoaded } = groupSlice.actions;

export const groupReducer = groupSlice.reducer;

export default {
  group: groupReducer,
};
