import React, { FC } from 'react';
import { getBackendSrv } from '@grafana/runtime';
import Page from 'app/core/components/Page/Page';
import { Button, Input, Field, Form } from '@grafana/ui';
import { getConfig } from 'app/core/config';
import { StoreState } from 'app/types';
import { hot } from 'react-hot-loader';
import { connect } from 'react-redux';
import { NavModel } from '@grafana/data';
import { getNavModel } from '../../core/selectors/navModel';

const createGroup = async (newGroup: { name: string }) => {
  const result = await getBackendSrv().post('/api/groups/', newGroup);

  await getBackendSrv().post('/api/user/using/' + result.orgId);
  window.location.href = getConfig().appSubUrl + '/group';
};

const validateGroup = async (groupName: string) => {
  try {
    await getBackendSrv().get(`api/groups/name/${encodeURI(groupName)}`);
  } catch (error) {
    if (error.status === 404) {
      error.isHandled = true;
      return true;
    }
    return 'Something went wrong';
  }
  return 'Group already exists';
};

interface PropsWithState {
  navModel: NavModel;
}

interface CreateGroupFormDTO {
  name: string;
}

export const NewGroupPage: FC<PropsWithState> = ({ navModel }) => {
  return (
    <Page navModel={navModel}>
      <Page.Contents>
        <h3 className="page-sub-heading">New Group</h3>

        <p className="playlist-description">
          Each group contains their own dashboards, data sources and configuration, and cannot be shared between groups.
          While users may belong to more than one, multiple group are most frequently used in multi-tenant deployments.{' '}
        </p>

        <Form<CreateGroupFormDTO> onSubmit={createGroup}>
          {({ register, errors }) => {
            return (
              <>
                <Field label="Group name" invalid={!!errors.name} error={errors.name && errors.name.message}>
                  <Input
                    placeholder="Group. name"
                    name="name"
                    ref={register({
                      required: 'Group name is required',
                      validate: async (groupName) => await validateGroup(groupName),
                    })}
                  />
                </Field>
                <Button type="submit">Create</Button>
              </>
            );
          }}
        </Form>
      </Page.Contents>
    </Page>
  );
};

const mapStateToProps = (state: StoreState) => {
  return { navModel: getNavModel(state.navIndex, 'global-groups') };
};

export default hot(module)(connect(mapStateToProps)(NewGroupPage));
