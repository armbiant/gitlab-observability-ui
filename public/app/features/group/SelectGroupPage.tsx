import React, { FC, useState } from 'react';
import Page from 'app/core/components/Page/Page';
import { getBackendSrv, config } from '@grafana/runtime';
import { UserGroup } from 'app/types';
import { useAsync } from 'react-use';
import { Button, HorizontalGroup } from '@grafana/ui';

const navModel = {
  main: {
    icon: 'gitLab',
    subTitle: 'Preferences',
    text: 'Select active group',
  },
  node: {
    text: 'Select active group',
  },
};

const getUserGroups = async () => {
  return await getBackendSrv().get('/api/user/groups');
};
const setUserGroup = async (group: UserGroup) => {
  return await getBackendSrv()
    .post('/api/user/using/' + group.groupId)
    .then(() => {
      window.location.href = config.appSubUrl + '/';
    });
};

export const SelectGroupPage: FC = () => {
  const [groups, setGroups] = useState<UserGroup[]>();

  useAsync(async () => {
    setGroups(await getUserGroups());
  }, []);
  return (
    <Page navModel={navModel}>
      <Page.Contents>
        <div>
          <p>
            You have been added to another Group due to an open invitation! Please select which group you want to use
            right now (you can change this later at any time).
          </p>
          <HorizontalGroup wrap>
            {groups &&
              groups.map((group) => (
                <Button key={group.groupId} icon="signin" onClick={() => setUserGroup(group)}>
                  {group.name}
                </Button>
              ))}
          </HorizontalGroup>
        </div>
      </Page.Contents>
    </Page>
  );
};

export default SelectGroupPage;
