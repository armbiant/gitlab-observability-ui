import React, { PureComponent } from 'react';
import { css, cx } from 'emotion';
import {
  Button,
  ConfirmButton,
  Container,
  Field,
  HorizontalGroup,
  Modal,
  stylesFactory,
  Themeable,
  withTheme,
} from '@grafana/ui';
import { GrafanaTheme } from '@grafana/data';
import { Group, GroupRole, UserGroup } from 'app/types';
import { GroupPicker, GroupSelectItem } from 'app/core/components/Select/GroupPicker';
import { GroupRolePicker } from './GroupRolePicker';

interface Props {
  groups: UserGroup[];

  onGroupRemove: (orgId: number) => void;
  onGroupRoleChange: (orgId: number, newRole: GroupRole) => void;
  onGroupAdd: (orgId: number, role: GroupRole) => void;
}

interface State {
  showAddGroupModal: boolean;
}

export class UserGroups extends PureComponent<Props, State> {
  state = {
    showAddGroupModal: false,
  };

  showGroupAddModal = (show: boolean) => () => {
    this.setState({ showAddGroupModal: show });
  };

  render() {
    const { groups, onGroupRoleChange, onGroupRemove, onGroupAdd } = this.props;
    const { showAddGroupModal } = this.state;
    const addToGroupContainerClass = css`
      margin-top: 0.8rem;
    `;

    return (
      <>
        <h3 className="page-heading">Groups</h3>
        <div className="gf-form-group">
          <div className="gf-form">
            <table className="filter-table form-inline">
              <tbody>
                {groups.map((group, index) => (
                  <GroupRow
                    key={`${group.groupId}-${index}`}
                    group={group}
                    onGroupRoleChange={onGroupRoleChange}
                    onGroupRemove={onGroupRemove}
                  />
                ))}
              </tbody>
            </table>
          </div>
          <div className={addToGroupContainerClass}>
            <Button variant="secondary" onClick={this.showGroupAddModal(true)}>
              Add user to group
            </Button>
          </div>
          <AddToGroupModal
            isOpen={showAddGroupModal}
            onGroupAdd={onGroupAdd}
            onDismiss={this.showGroupAddModal(false)}
          />
        </div>
      </>
    );
  }
}

const getGroupRowStyles = stylesFactory((theme: GrafanaTheme) => {
  return {
    removeButton: css`
      margin-right: 0.6rem;
      text-decoration: underline;
      color: ${theme.palette.blue95};
    `,
    label: css`
      font-weight: 500;
    `,
  };
});

interface GroupRowProps extends Themeable {
  group: UserGroup;
  onGroupRemove: (orgId: number) => void;
  onGroupRoleChange: (orgId: number, newRole: string) => void;
}

interface GroupRowState {
  currentRole: GroupRole;
  isChangingRole: boolean;
}

class UnThemedGroupRow extends PureComponent<GroupRowProps, GroupRowState> {
  state = {
    currentRole: this.props.group.role,
    isChangingRole: false,
  };

  onGroupRemove = () => {
    const { group } = this.props;
    this.props.onGroupRemove(group.groupId);
  };

  onChangeRoleClick = () => {
    const { group } = this.props;
    this.setState({ isChangingRole: true, currentRole: group.role });
  };

  onGroupRoleChange = (newRole: GroupRole) => {
    this.setState({ currentRole: newRole });
  };

  onGroupRoleSave = () => {
    this.props.onGroupRoleChange(this.props.group.groupId, this.state.currentRole);
  };

  onCancelClick = () => {
    this.setState({ isChangingRole: false });
  };

  render() {
    const { group, theme } = this.props;
    const { currentRole, isChangingRole } = this.state;
    const styles = getGroupRowStyles(theme);
    const labelClass = cx('width-16', styles.label);

    return (
      <tr>
        <td className={labelClass}>{group.name}</td>
        {isChangingRole ? (
          <td>
            <GroupRolePicker value={currentRole} onChange={this.onGroupRoleChange} />
          </td>
        ) : (
          <td className="width-25">{group.role}</td>
        )}
        <td colSpan={1}>
          <div className="pull-right">
            <ConfirmButton
              confirmText="Save"
              onClick={this.onChangeRoleClick}
              onCancel={this.onCancelClick}
              onConfirm={this.onGroupRoleSave}
            >
              Change role
            </ConfirmButton>
          </div>
        </td>
        <td colSpan={1}>
          <div className="pull-right">
            <ConfirmButton
              confirmText="Confirm removal"
              confirmVariant="destructive"
              onCancel={this.onCancelClick}
              onConfirm={this.onGroupRemove}
            >
              Remove from group
            </ConfirmButton>
          </div>
        </td>
      </tr>
    );
  }
}

const GroupRow = withTheme(UnThemedGroupRow);

const getAddToGroupModalStyles = stylesFactory(() => ({
  modal: css`
    width: 500px;
  `,
  buttonRow: css`
    text-align: center;
  `,
  modalContent: css`
    overflow: visible;
  `,
}));

interface AddToGroupModalProps {
  isOpen: boolean;
  onGroupAdd(orgId: number, role: string): void;
  onDismiss?(): void;
}

interface AddToGroupModalState {
  selectedGroup: Group | null;
  role: GroupRole;
}

export class AddToGroupModal extends PureComponent<AddToGroupModalProps, AddToGroupModalState> {
  state: AddToGroupModalState = {
    selectedGroup: null,
    role: GroupRole.Admin,
  };

  onGroupSelect = (group: GroupSelectItem) => {
    this.setState({ selectedGroup: { ...group } });
  };

  onGroupRoleChange = (newRole: GroupRole) => {
    this.setState({
      role: newRole,
    });
  };

  onAddUserToGroup = () => {
    const { selectedGroup, role } = this.state;
    this.props.onGroupAdd(selectedGroup!.id, role);
  };

  onCancel = () => {
    if (this.props.onDismiss) {
      this.props.onDismiss();
    }
  };

  render() {
    const { isOpen } = this.props;
    const { role } = this.state;
    const styles = getAddToGroupModalStyles();
    return (
      <Modal
        className={styles.modal}
        contentClassName={styles.modalContent}
        title="Add to an group"
        isOpen={isOpen}
        onDismiss={this.onCancel}
      >
        <Field label="Group">
          <GroupPicker onSelected={this.onGroupSelect} />
        </Field>
        <Field label="Role">
          <GroupRolePicker value={role} onChange={this.onGroupRoleChange} />
        </Field>
        <Container padding="md">
          <HorizontalGroup spacing="md" justify="center">
            <Button variant="primary" onClick={this.onAddUserToGroup}>
              Add to group
            </Button>
            <Button variant="secondary" onClick={this.onCancel}>
              Cancel
            </Button>
          </HorizontalGroup>
        </Container>
      </Modal>
    );
  }
}
