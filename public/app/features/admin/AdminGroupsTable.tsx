import React, { FC, useState } from 'react';
import { Group } from 'app/types';
import { Button, ConfirmModal } from '@grafana/ui';

interface Props {
  groups: Group[];
  onDelete: (orgId: number) => void;
}

export const AdminGroupsTable: FC<Props> = ({ groups, onDelete }) => {
  const [deleteGroup, setDeleteGroup] = useState<Group>();
  return (
    <table className="filter-table form-inline filter-table--hover">
      <thead>
        <tr>
          <th>Id</th>
          <th>Name</th>
          <th style={{ width: '1%' }}></th>
        </tr>
      </thead>
      <tbody>
        {groups.map((group) => (
          <tr key={`${group.id}-${group.name}`}>
            <td className="link-td">
              <a href={`admin/groups/edit/${group.id}`}>{group.id}</a>
            </td>
            <td className="link-td">
              <a href={`admin/groups/edit/${group.id}`}>{group.name}</a>
            </td>
            <td className="text-right">
              <Button variant="destructive" size="sm" icon="times" onClick={() => setDeleteGroup(group)} />
            </td>
          </tr>
        ))}
      </tbody>
      {deleteGroup && (
        <ConfirmModal
          isOpen
          icon="trash-alt"
          title="Delete"
          body={
            <div>
              Are you sure you want to delete &apos;{deleteGroup.name}&apos;?
              <br /> <small>All dashboards for this group will be removed!</small>
            </div>
          }
          confirmText="Delete"
          onDismiss={() => setDeleteGroup(undefined)}
          onConfirm={() => {
            onDelete(deleteGroup.id);
            setDeleteGroup(undefined);
          }}
        />
      )}
    </table>
  );
};
