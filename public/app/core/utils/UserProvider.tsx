import React, { PureComponent } from 'react';
import { getBackendSrv } from '@grafana/runtime';
import { UserDTO, Team, UserGroup, UserSession } from 'app/types';
import { config } from 'app/core/config';
import { dateTimeFormat, dateTimeFormatTimeAgo } from '@grafana/data';

export interface UserAPI {
  changePassword: (changePassword: ChangePasswordFields) => void;
  updateUserProfile: (profile: ProfileUpdateFields) => void;
  loadUser: () => void;
  loadTeams: () => void;
  loadGroups: () => void;
  loadSessions: () => void;
  setUserGroup: (group: UserGroup) => void;
  revokeUserSession: (tokenId: number) => void;
}

export interface LoadingStates {
  changePassword: boolean;
  loadUser: boolean;
  loadTeams: boolean;
  loadGroups: boolean;
  loadSessions: boolean;
  updateUserProfile: boolean;
  updateUserGroup: boolean;
}

export interface ChangePasswordFields {
  oldPassword: string;
  newPassword: string;
  confirmNew: string;
}

export interface ProfileUpdateFields {
  name: string;
  email: string;
  login: string;
}

export interface Props {
  userId?: number; // passed, will load user on mount
  children: (
    api: UserAPI,
    states: LoadingStates,
    teams: Team[],
    groups: UserGroup[],
    sessions: UserSession[],
    user?: UserDTO
  ) => JSX.Element;
}

export interface State {
  user?: UserDTO;
  teams: Team[];
  groups: UserGroup[];
  sessions: UserSession[];
  loadingStates: LoadingStates;
}

export class UserProvider extends PureComponent<Props, State> {
  state: State = {
    teams: [] as Team[],
    groups: [] as UserGroup[],
    sessions: [] as UserSession[],
    loadingStates: {
      changePassword: false,
      loadUser: true,
      loadTeams: false,
      loadGroups: false,
      loadSessions: false,
      updateUserProfile: false,
      updateUserGroup: false,
    },
  };

  UNSAFE_componentWillMount() {
    if (this.props.userId) {
      this.loadUser();
    }
  }

  changePassword = async (payload: ChangePasswordFields) => {
    this.setState({ loadingStates: { ...this.state.loadingStates, changePassword: true } });
    await getBackendSrv().put('/api/user/password', payload);
    this.setState({ loadingStates: { ...this.state.loadingStates, changePassword: false } });
  };

  loadUser = async () => {
    this.setState({
      loadingStates: { ...this.state.loadingStates, loadUser: true },
    });
    const user = await getBackendSrv().get('/api/user');
    this.setState({ user, loadingStates: { ...this.state.loadingStates, loadUser: Object.keys(user).length === 0 } });
  };

  loadTeams = async () => {
    this.setState({
      loadingStates: { ...this.state.loadingStates, loadTeams: true },
    });
    const teams = await getBackendSrv().get('/api/user/teams');
    this.setState({ teams, loadingStates: { ...this.state.loadingStates, loadTeams: false } });
  };

  loadGroups = async () => {
    this.setState({
      loadingStates: { ...this.state.loadingStates, loadGroups: true },
    });
    const groups = await getBackendSrv().get('/api/user/groups');
    this.setState({ groups, loadingStates: { ...this.state.loadingStates, loadGroups: false } });
  };

  loadSessions = async () => {
    this.setState({
      loadingStates: { ...this.state.loadingStates, loadSessions: true },
    });

    await getBackendSrv()
      .get('/api/user/auth-tokens')
      .then((sessions: UserSession[]) => {
        sessions = sessions
          // Show active sessions first
          .sort((a, b) => Number(b.isActive) - Number(a.isActive))
          .map((session: UserSession) => {
            return {
              id: session.id,
              isActive: session.isActive,
              seenAt: dateTimeFormatTimeAgo(session.seenAt),
              createdAt: dateTimeFormat(session.createdAt, { format: 'MMMM DD, YYYY' }),
              clientIp: session.clientIp,
              browser: session.browser,
              browserVersion: session.browserVersion,
              os: session.os,
              osVersion: session.osVersion,
              device: session.device,
            };
          });

        this.setState({ sessions, loadingStates: { ...this.state.loadingStates, loadSessions: false } });
      });
  };

  revokeUserSession = async (tokenId: number) => {
    await getBackendSrv()
      .post('/api/user/revoke-auth-token', {
        authTokenId: tokenId,
      })
      .then(() => {
        const sessions = this.state.sessions.filter((session: UserSession) => {
          return session.id !== tokenId;
        });

        this.setState({ sessions });
      });
  };

  setUserGroup = async (group: UserGroup) => {
    this.setState({
      loadingStates: { ...this.state.loadingStates, updateUserGroup: true },
    });
    await getBackendSrv()
      .post('/api/user/using/' + group.groupId, {})
      .then(() => {
        window.location.href = config.appSubUrl + '/profile';
      })
      .finally(() => {
        this.setState({ loadingStates: { ...this.state.loadingStates, updateUserGroup: false } });
      });
  };

  updateUserProfile = async (payload: ProfileUpdateFields) => {
    this.setState({ loadingStates: { ...this.state.loadingStates, updateUserProfile: true } });
    await getBackendSrv()
      .put('/api/user', payload)
      .then(this.loadUser)
      .catch((e) => console.error(e))
      .finally(() => {
        this.setState({ loadingStates: { ...this.state.loadingStates, updateUserProfile: false } });
      });
  };

  render() {
    const { children } = this.props;
    const { loadingStates, teams, groups, sessions, user } = this.state;

    const api: UserAPI = {
      changePassword: this.changePassword,
      loadUser: this.loadUser,
      loadTeams: this.loadTeams,
      loadGroups: this.loadGroups,
      loadSessions: this.loadSessions,
      revokeUserSession: this.revokeUserSession,
      updateUserProfile: this.updateUserProfile,
      setUserGroup: this.setUserGroup,
    };

    return <>{children(api, loadingStates, teams, groups, sessions, user)}</>;
  }
}

export default UserProvider;
