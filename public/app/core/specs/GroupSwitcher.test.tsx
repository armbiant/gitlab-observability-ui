import React from 'react';
import { GroupSwitcher } from '../components/GroupSwitcher';
import { shallow } from 'enzyme';
import { GroupRole } from '@grafana/data';

const postMock = jest.fn().mockImplementation(jest.fn());

jest.mock('@grafana/runtime', () => ({
  getBackendSrv: () => ({
    get: jest.fn().mockResolvedValue([]),
    post: postMock,
  }),
}));

jest.mock('app/core/services/context_srv', () => ({
  contextSrv: {
    user: { orgId: 1 },
  },
}));

jest.mock('app/core/config', () => {
  return {
    appSubUrl: '/subUrl',
  };
});

let wrapper;
let groupSwitcher: GroupSwitcher;

describe('GroupSwitcher', () => {
  describe('when switching group', () => {
    beforeEach(async () => {
      wrapper = shallow(<GroupSwitcher onDismiss={() => {}} />);
      groupSwitcher = wrapper.instance() as GroupSwitcher;
      groupSwitcher.setWindowLocation = jest.fn();
      wrapper.update();
      await groupSwitcher.setCurrentGroup({ name: 'mock group', groupId: 2, role: GroupRole.Viewer });
    });

    it('should switch orgId in call to backend', () => {
      expect(postMock).toBeCalledWith('/api/user/using/2');
    });

    it('should switch orgId in url and redirect to home page', () => {
      expect(groupSwitcher.setWindowLocation).toBeCalledWith('/subUrl/?groupId=2');
    });
  });
});
