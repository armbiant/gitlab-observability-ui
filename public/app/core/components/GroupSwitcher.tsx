import React from 'react';

import { getBackendSrv } from '@grafana/runtime';
import { UserGroupDTO } from '@grafana/data';
import { Modal, Button } from '@grafana/ui';

import { contextSrv } from 'app/core/services/context_srv';
import config from 'app/core/config';

interface Props {
  onDismiss: () => void;
}

interface State {
  groups: UserGroupDTO[];
}

export class GroupSwitcher extends React.PureComponent<Props, State> {
  state: State = {
    groups: [],
  };

  componentDidMount() {
    this.getUserGroups();
  }

  getUserGroups = async () => {
    const groups: UserGroupDTO[] = await getBackendSrv().get('/api/user/groups');
    this.setState({ groups });
  };

  setCurrentGroup = async (group: UserGroupDTO) => {
    await getBackendSrv().post(`/api/user/using/${group.groupId}`);
    this.setWindowLocation(`${config.appSubUrl}${config.appSubUrl.endsWith('/') ? '' : '/'}?groupId=${group.groupId}`);
  };

  setWindowLocation(href: string) {
    window.location.href = href;
  }

  render() {
    const { onDismiss } = this.props;
    const { groups } = this.state;

    const currentGroupId = contextSrv.user.orgId;

    return (
      <Modal title="Switch Group" icon="arrow-random" onDismiss={onDismiss} isOpen={true}>
        <table className="filter-table form-inline">
          <thead>
            <tr>
              <th>Name</th>
              <th>Role</th>
              <th />
            </tr>
          </thead>
          <tbody>
            {groups.map((group) => (
              <tr key={group.groupId}>
                <td>{group.name}</td>
                <td>{group.role}</td>
                <td className="text-right">
                  {group.groupId === currentGroupId ? (
                    <Button size="sm">Current</Button>
                  ) : (
                    <Button variant="secondary" size="sm" onClick={() => this.setCurrentGroup(group)}>
                      Switch to
                    </Button>
                  )}
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </Modal>
    );
  }
}
