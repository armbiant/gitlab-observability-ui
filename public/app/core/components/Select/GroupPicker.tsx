import React, { PureComponent } from 'react';
import { AsyncSelect } from '@grafana/ui';
import { getBackendSrv } from 'app/core/services/backend_srv';
import { Group } from 'app/types';
import { SelectableValue } from '@grafana/data';

export interface GroupSelectItem {
  id: number;
  value: number;
  label: string;
  name: string;
}

export interface Props {
  onSelected: (group: GroupSelectItem) => void;
  className?: string;
}

export interface State {
  isLoading: boolean;
}

export class GroupPicker extends PureComponent<Props, State> {
  groups: Group[];

  state: State = {
    isLoading: false,
  };

  async loadGroups() {
    this.setState({ isLoading: true });
    const groups = await getBackendSrv().get('/api/groups');
    this.groups = groups;
    this.setState({ isLoading: false });
    return groups;
  }

  getGroupOptions = async (query: string): Promise<Array<SelectableValue<number>>> => {
    if (!this.groups) {
      await this.loadGroups();
    }
    return this.groups.map(
      (group: Group): SelectableValue<number> => ({
        id: group.id,
        value: group.id,
        label: group.name,
        name: group.name,
      })
    );
  };

  render() {
    const { className, onSelected } = this.props;
    const { isLoading } = this.state;

    return (
      <AsyncSelect
        className={className}
        isLoading={isLoading}
        defaultOptions={true}
        isSearchable={false}
        loadOptions={this.getGroupOptions}
        onChange={onSelected}
        placeholder="Select group"
        noOptionsMessage="No groups found"
      />
    );
  }
}
