# Graph Panel

This is the main Graph panel and is **included** with GitLab Observability UI. It provides a very rich set of graphing options.