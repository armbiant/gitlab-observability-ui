# Dashlist Panel -  Native Plugin

This Dashlist panel is **included** with GitLab Observability UI.

The dashboard list panel allows you to display dynamic links to other dashboards. The list can be configured to use starred dashboards, a search query and/or dashboard tags.