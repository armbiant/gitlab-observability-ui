# Plugin List Panel -  Native Plugin

The Plugin List plans shows the installed plugins for your GitLab Observability UI instance and is **included** with GitLab Observability UI. It is used on the default Home dashboard.
