# Annotation List Panel -  Native Plugin

This Annotations List panel is **included** with GitLab Observability UI.

However, it is presently in alpha and is not accessible unless alpha panels are enabled.