export interface Group {
  name: string;
  id: number;
}

export interface GroupState {
  group: Group;
}
