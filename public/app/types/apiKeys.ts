﻿import { GroupRole } from './acl';

export interface ApiKey {
  id?: number;
  name: string;
  role: GroupRole;
  secondsToLive: number | null;
  expiration?: string;
}

export interface NewApiKey {
  name: string;
  role: GroupRole;
  secondsToLive: string;
}

export interface ApiKeysState {
  keys: ApiKey[];
  searchQuery: string;
  hasFetched: boolean;
}
