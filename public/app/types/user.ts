import { TimeZone } from '@grafana/data';
import { GroupRole } from '.';

export interface GroupUser {
  avatarUrl: string;
  email: string;
  lastSeenAt: string;
  lastSeenAtAge: string;
  login: string;
  name: string;
  groupId: number;
  role: GroupRole;
  userId: number;
}

export interface User {
  id: number;
  label: string;
  avatarUrl: string;
  login: string;
  email: string;
  name: string;
  groupId?: number;
}

export interface UserDTO {
  id: number;
  login: string;
  email: string;
  name: string;
  isGrafanaAdmin: boolean;
  isDisabled: boolean;
  isAdmin?: boolean;
  isExternal?: boolean;
  updatedAt?: string;
  authLabels?: string[];
  theme?: string;
  avatarUrl?: string;
  groupId?: number;
  lastSeenAtAge?: string;
}

export interface Invitee {
  code: string;
  createdOn: string;
  email: string;
  emailSent: boolean;
  emailSentOn: string;
  id: number;
  invitedByEmail: string;
  invitedByLogin: string;
  invitedByName: string;
  name: string;
  groupId: number;
  role: string;
  status: string;
  url: string;
}

export interface UsersState {
  users: GroupUser[];
  invitees: Invitee[];
  searchQuery: string;
  searchPage: number;
  canInvite: boolean;
  externalUserMngLinkUrl: string;
  externalUserMngLinkName: string;
  externalUserMngInfo: string;
  hasFetched: boolean;
}

export interface UserState {
  groupId: number;
  timeZone: TimeZone;
}

export interface UserSession {
  id: number;
  createdAt: string;
  clientIp: string;
  isActive: boolean;
  seenAt: string;
  browser: string;
  browserVersion: string;
  os: string;
  osVersion: string;
  device: string;
}

export interface UserGroup {
  name: string;
  groupId: number;
  role: GroupRole;
}

export interface UserAdminState {
  user: UserDTO | null;
  sessions: UserSession[];
  groups: UserGroup[];
  isLoading: boolean;
  error?: UserAdminError | null;
}

export interface UserAdminError {
  title: string;
  body: string;
}

export interface UserListAdminState {
  users: UserDTO[];
  query: string;
  perPage: number;
  page: number;
  totalPages: number;
  showPaging: boolean;
}
