# Reporting security issues

Thanks for considering to report a security problem! Please follow the [responsible disclosure policy](https://about.gitlab.com/security/disclosure/).
