# GitLab Observability UI - Release Strategy

GitLab Observability UI (GOUI) is bundled and deployed together with the full Opstrace stack to http://staging.observe.gitlab.com / http://observe.gitlab.com, and embedded into GitLab UI through an iframe. 

The release and deployment of the UI is currently responsibility of Optrace.

GOUI CI/CD pipeline is responsible for building, testing and packaging the UI stack as a Docker image, and eventually publishing it to the GitLab Container Registry. The pipeline also runs e2e tests for the UI only ( do not include integration tests with Opstrace or GitLab).

Opstrace currently relies on a manual process to release the latest UI, which includes manually updating the [Docker image SHA](https://gitlab.com/gitlab-org/opstrace/opstrace/-/blob/main/go/pkg/constants/docker-images.json#L3) to the latest version, test it locally and creating a MR for it. This process will eventually be automated, once full e2e tests are in place.

Discussion: https://gitlab.com/gitlab-org/opstrace/opstrace-ui/-/issues/43