+++
title = "Group HTTP API "
description = "Grafana Group HTTP API"
keywords = ["grafana", "http", "documentation", "api", "organization"]
aliases = ["/docs/grafana/latest/http_api/organization/"]
+++

# Group API

The Group HTTP API is divided in two resources, `/api/group` (current organization)
and `/api/groups` (admin organizations). One big difference between these are that
the admin of all organizations API only works with basic authentication, see [Admin Groups API](#admin-organizations-api) for more information.

## Current Group API

### Get current Group

`GET /api/group/`

**Example Request**:

```http
GET /api/group/ HTTP/1.1
Accept: application/json
Content-Type: application/json
Authorization: Bearer eyJrIjoiT0tTcG1pUlY2RnVKZTFVaDFsNFZXdE9ZWmNrMkZYbk
```

**Example Response**:

```http
HTTP/1.1 200
Content-Type: application/json

{
  "id":1,
  "name":"Main Group."
}
```

### Get all users within the current organization

`GET /api/group/users`

Returns all org users within the current organization.
Accessible to users with org admin role.

**Example Request**:

```http
GET /api/group/users HTTP/1.1
Accept: application/json
Content-Type: application/json
Authorization: Bearer eyJrIjoiT0tTcG1pUlY2RnVKZTFVaDFsNFZXdE9ZWmNrMkZYbk
```

**Example Response**:

```http
HTTP/1.1 200
Content-Type: application/json

[
  {
    "orgId": 1,
    "userId": 1,
    "email": "admin@localhost",
    "avatarUrl": "/avatar/46d229b033af06a191ff2267bca9ae56",
    "login": "admin",
    "role": "Admin",
    "lastSeenAt": "2019-08-09T11:02:49+02:00",
    "lastSeenAtAge": "< 1m"
  }
]
```

### Get all users within the current organization (lookup)

`GET /api/group/users/lookup`

Returns all org users within the current organization, but with less detailed information.
Accessible to users with org admin role, admin in any folder or admin of any team.
Mainly used by Grafana UI for providing list of users when adding team members and
when editing folder/dashboard permissions.

**Example Request**:

```http
GET /api/group/users/lookup HTTP/1.1
Accept: application/json
Content-Type: application/json
Authorization: Bearer eyJrIjoiT0tTcG1pUlY2RnVKZTFVaDFsNFZXdE9ZWmNrMkZYbk
```

**Example Response**:

```http
HTTP/1.1 200
Content-Type: application/json

[
  {
    "userId": 1,
    "login": "admin",
    "avatarUrl": "/avatar/46d229b033af06a191ff2267bca9ae56"
  }
]
```

### Updates the given user

`PATCH /api/group/users/:userId`

**Example Request**:

```http
PATCH /api/group/users/1 HTTP/1.1
Accept: application/json
Content-Type: application/json
Authorization: Bearer eyJrIjoiT0tTcG1pUlY2RnVKZTFVaDFsNFZXdE9ZWmNrMkZYbk

{
  "role": "Viewer",
}
```

**Example Response**:

```http
HTTP/1.1 200
Content-Type: application/json

{"message":"Group user updated"}
```

### Delete user in current organization

`DELETE /api/group/users/:userId`

**Example Request**:

```http
DELETE /api/group/users/1 HTTP/1.1
Accept: application/json
Content-Type: application/json
Authorization: Bearer eyJrIjoiT0tTcG1pUlY2RnVKZTFVaDFsNFZXdE9ZWmNrMkZYbk
```

**Example Response**:

```http
HTTP/1.1 200
Content-Type: application/json

{"message":"User removed from group"}
```

### Update current Group

`PUT /api/org`

**Example Request**:

```http
PUT /api/org HTTP/1.1
Accept: application/json
Content-Type: application/json
Authorization: Bearer eyJrIjoiT0tTcG1pUlY2RnVKZTFVaDFsNFZXdE9ZWmNrMkZYbk

{
  "name":"Main Group."
}
```

**Example Response**:

```http
HTTP/1.1 200
Content-Type: application/json

{"message":"Group updated"}
```

### Add a new user to the current group

`POST /api/group/users`

Adds a global user to the current group.

**Example Request**:

```http
POST /api/group/users HTTP/1.1
Accept: application/json
Content-Type: application/json
Authorization: Bearer eyJrIjoiT0tTcG1pUlY2RnVKZTFVaDFsNFZXdE9ZWmNrMkZYbk

{
  "role": "Admin",
  "loginOrEmail": "admin"
}
```

**Example Response**:

```http
HTTP/1.1 200
Content-Type: application/json

{"message":"User added to group","userId":1}
```

## Admin Groups API

The Admin Groups HTTP API does not currently work with an API Token. API Tokens are currently
only linked to an group and an group role. They cannot be given the permission of server
admin, only users can be given that permission. So in order to use these API calls you will have to
use Basic Auth and the Grafana user must have the Grafana Admin permission (The default admin user
is called `admin` and has permission to use this API).

### Get Group by Id

`GET /api/groups/:orgId`

Only works with Basic Authentication (username and password), see [introduction](#admin-organizations-api).

**Example Request**:

```http
GET /api/groups/1 HTTP/1.1
Accept: application/json
Content-Type: application/json
```

**Example Response**:

```http
HTTP/1.1 200
Content-Type: application/json

{
  "id":1,
  "name":"Main Group.",
  "address":{
    "address1":"",
    "address2":"",
    "city":"",
    "zipCode":"",
    "state":"",
    "country":""
  }
}
```

### Get Group by Name

`GET /api/groups/name/:orgName`

Only works with Basic Authentication (username and password), see [introduction](#admin-organizations-api).

**Example Request**:

```http
GET /api/groups/name/Main%20Group%2E HTTP/1.1
Accept: application/json
Content-Type: application/json
```

**Example Response**:

```http
HTTP/1.1 200
Content-Type: application/json

{
  "id":1,
  "name":"Main Group.",
  "address":{
    "address1":"",
    "address2":"",
    "city":"",
    "zipCode":"",
    "state":"",
    "country":""
  }
}
```

### Create Group

`POST /api/groups`

Only works with Basic Authentication (username and password), see [introduction](#admin-organizations-api).

**Example Request**:

```http
POST /api/groups HTTP/1.1
Accept: application/json
Content-Type: application/json

{
  "name":"New Group."
}
```

Note: The api will work in the following two ways

1. Need to set GF_USERS_ALLOW_ORG_CREATE=true
2. Set the config value users.allow_org_create to true in ini file

**Example Response**:

```http
HTTP/1.1 200
Content-Type: application/json

{
  "orgId":"1",
  "message":"Group created"
}
```

### Search all Groups

`GET /api/groups?perpage=10&page=1`

Only works with Basic Authentication (username and password), see [introduction](#admin-organizations-api).

**Example Request**:

```http
GET /api/groups HTTP/1.1
Accept: application/json
Content-Type: application/json
```

Note: The api will only work when you pass the admin name and password
to the request HTTP URL, like http://admin:admin@localhost:3000/api/groups

Default value for the `perpage` parameter is `1000` and for the `page` parameter is `0`.

**Example Response**:

```http
HTTP/1.1 200
Content-Type: application/json

[
  {
    "id":1,
    "name":"Main Group."
  }
]
```

### Update Group

`PUT /api/groups/:orgId`

Update Group, fields _Address 1_, _Address 2_, _City_ are not implemented yet.
Only works with Basic Authentication (username and password), see [introduction](#admin-organizations-api).

**Example Request**:

```http
PUT /api/groups/1 HTTP/1.1
Accept: application/json
Content-Type: application/json

{
  "name":"Main Group 2."
}
```

**Example Response**:

```http
HTTP/1.1 200
Content-Type: application/json

{"message":"Group updated"}
```

### Delete Group

`DELETE /api/groups/:orgId`

Only works with Basic Authentication (username and password), see [introduction](#admin-organizations-api).

**Example Request**:

```http
DELETE /api/groups/1 HTTP/1.1
Accept: application/json
```

**Example Response**:

```http
HTTP/1.1 200
Content-Type: application/json

{"message":"Group deleted"}
```

### Get Users in Group

`GET /api/groups/:orgId/users`

Only works with Basic Authentication (username and password), see [introduction](#admin-organizations-api).

**Example Request**:

```http
GET /api/groups/1/users HTTP/1.1
Accept: application/json
Content-Type: application/json
```

Note: The api will only work when you pass the admin name and password
to the request HTTP URL, like http://admin:admin@localhost:3000/api/groups/1/users

**Example Response**:

```http
HTTP/1.1 200
Content-Type: application/json
[
  {
    "orgId":1,
    "userId":1,
    "email":"admin@mygraf.com",
    "login":"admin",
    "role":"Admin"
  }
]
```

### Add User in Group

`POST /api/groups/:orgId/users`

Only works with Basic Authentication (username and password), see [introduction](#admin-organizations-api).

**Example Request**:

```http
POST /api/groups/1/users HTTP/1.1
Accept: application/json
Content-Type: application/json

{
  "loginOrEmail":"user",
  "role":"Viewer"
}
```

**Example Response**:

```http
HTTP/1.1 200
Content-Type: application/json

{"message":"User added to organization", "userId": 1}
```

### Update Users in Group

`PATCH /api/groups/:orgId/users/:userId`

Only works with Basic Authentication (username and password), see [introduction](#admin-organizations-api).

**Example Request**:

```http
PATCH /api/groups/1/users/2 HTTP/1.1
Accept: application/json
Content-Type: application/json

{
  "role":"Admin"
}
```

**Example Response**:

```http
HTTP/1.1 200
Content-Type: application/json

{"message":"Group user updated"}
```

### Delete User in Group

`DELETE /api/groups/:orgId/users/:userId`

Only works with Basic Authentication (username and password), see [introduction](#admin-organizations-api).

**Example Request**:

```http
DELETE /api/groups/1/users/2 HTTP/1.1
Accept: application/json
Content-Type: application/json
```

**Example Response**:

```http
HTTP/1.1 200
Content-Type: application/json

{"message":"User removed from group"}
```
