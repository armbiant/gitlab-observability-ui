# Contributing to Opstrace

Considering contributing to Opstrace?
We'd love work with you!

As a starting point,

* please visit the Opstrace [contribution guide](./docs/sources/developer/), and
* have a look at our [development environment](./docs/sources/developer/setting-up-your-dev-env.md) documentation.

Do join us for [discussions in our community](https://go.opstrace.com/community).

## Code of Conduct

We pledge to follow the [GitLab Community Code of Conduct](https://about.gitlab.com/community/contribute/code-of-conduct/).

