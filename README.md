# GitLab Observability UI

The GitLab Observability UI provides a solid base to build upon and create a place where we can rapidly iterate on new use cases and ways to query and display data regardless of the type.

Additional features and functionality will include:

- Apache License, Version 2.0: Enables the UI to be embedded in GitLab and other companies’ existing products.
- Prometheus focused: Native Alerts and rules for Alert Manager.
- User Interface/User Experience (UI/UX) Features: Allows Opstrace to explore new UI/UX features without worrying about retro compatibility.
- Keyboard Driven: Ability to navigate with keyboard commands, shortcuts, keystrokes in addition to using the mouse/trackpad.
- Grafana Dashboard: Basic Grafana dashboard compatibility.
- Integration: Ease of integration into GitLab and ability to embed in other UIs.
- Innovation: The monitor and observability landscape for DevOps has evolved over the years with new user and business needs. GitLab Observability UI will allow for a unified experience around metrics, logs, traces and any other kind of observability event.

Please follow the updates on the [Opstrace blog](https://opstrace.com/blog) and join the discussion in the issues.

## Contributing

Thanks for considering to contribute! Start your journey here:

- Start by reading the [Developer guide](contribute/developer-guide.md) and other guides under `contribute/`.
- Read the [glossary](contribute/glossary.md) to familiarize yourself with the terminology.
- Learn how to set up your local environment, in our [community guide](https://about.gitlab.com/community/contribute/).
- Explore our [issues](https://gitlab.com/groups/gitlab-org/opstrace/-/issues?sort=updated_desc&state=opened).

## Get involved

- Follow [@opstrace](https://twitter.com/opstrace) and [@gitlab](https://twitter.com/gitlab) on Twitter
- Read and subscribe to the [Opstrace](https://opstrace.com/blog) and [GitLab blog](https://about.gitlab.com/blog/).
- If you have a specific question, check out our [community forums](https://forum.gitlab.com/).

## License

This project is distributed under the [Apache 2.0 License](LICENSE).
